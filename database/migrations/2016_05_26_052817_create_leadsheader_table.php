<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsheaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("leadsheader", function(Blueprint $table){
            $table->increments("id");
            $table->integer("lead_id")->unsigned();
            $table->string("host");
            $table->string("user_agent");
            $table->string("referer");
            $table->string("browser");
            $table->string("operating_system");
            $table->string("device_type");
            $table->timestamps();

           $table->foreign("lead_id")
                ->references("id")
                ->on("leads")
                ->onDelete("cascade"); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("leadsheader");
    }
}
