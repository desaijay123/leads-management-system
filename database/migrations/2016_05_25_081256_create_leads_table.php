<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create("leads", function(Blueprint $table){
            $table->increments("id");
            $table->integer("campaign_id")->unsigned();
            $table->integer("brand_id")->unsigned();
            $table->string("customer_name");
            $table->string("age");
            $table->string("gender");
            $table->string("city");
            $table->string("email");
            $table->string("contact");
            $table->string("source");
            $table->string("campaign_name");
            $table->string("ad_group");
            $table->string("keyword");
            $table->string("ad");
            $table->string("referal");
            $table->string("push_url");
            $table->string("push_status");
            $table->string("car_brand");
            $table->string("member");
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("leads");
    }
}
