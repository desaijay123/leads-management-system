<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create("brands",function(Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->string("label")->nullable();
            $table->timestamps();
        });

        Schema::create("brand_user", function(Blueprint $table){
            $table->increments("id");
            $table->integer("brand_id")->unsigned()->nullable();
            $table->integer("user_id")->unsigned()->nullable();
            $table->integer("campaign_id")->unsigned()->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("brand_user");
        Schema::drop("brands");

    }
}
