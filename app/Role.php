<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	protected $table = "roles";
	protected $fillable = ["name","label"];

public function addRole(){
		return $this->save();
	}
    public function permissions(){
		return $this->belongsToMany(Permission::class);
	}

	public function givePermissionTo(Permission $permission){
		return $this->permissions()->attach($permission);
	}

	public function revokeTo(Permission $permission){
		return $this->permissions()->detach($permission);
	}

	public function users(){
		return $this->belongsToMany(User::class);
	}
}
