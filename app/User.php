<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Brand;
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

     public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    /**
     * Assign the given role to the user.
     *
     * @param  string $role
     * @return mixed
     */
    public function assignRole(Role $role)
    {
        return $this->roles()->attach($role);
    }
    /**
     * Determine if the user has the given role.
     *
     * @param  mixed $role
     * @return boolean
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }
       
        return !! $role->intersect($this->roles)->count();
    }
    /**
     * Determine if the user may perform the given permission.
     *
     * @param  Permission $permission
     * @return boolean
     */
    public function hasPermission(Permission $permission)
    {
        return $this->hasRole($permission->roles);
    }

    public function revokeRole(Role $role){
        return $this->roles()->detach($role);
    }

    public function brands(){
        return $this->belongsToMany(Brand::class);
    }

//check whether the user has particular brand

     public function hasBrand($branduser)
    {
        if (is_integer($branduser)) {
            return $this->branduser->contains('brand_id', $branduser);
        }
       
    }


    public function roleuser(){
        return $this->hasOne(RoleUser::class);
    }



    public function hasSuperadminrole(){
        return $this->hasRole("super_admin");
    }

    public function isSuperadmin(){
        return $this->hasSuperadminrole();
    }

    public function hasAccountmanagerrole(){
        return $this->hasRole("account_manager");
    }

    public function isAccountmanager(){
        return $this->hasAccountmanagerrole();
    }

    public function branduser(){
        return $this->belongsToMany("App\Brand","user_id");
    }


}
