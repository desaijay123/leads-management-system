<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leadsheader extends Model
{
	protected $table = "leadsheader";
    protected $fillable = ["lead_id","host","user_agent","referer","broowser","operating_system","device_type"];

    public function leads(){
    	return $this->belongsTo(Lead::class);
    }
}
