<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandUser extends Model
{
    protected $table = "brand_user";

    protected $fillable = ["brand_id","user_id"];

    public function users(){
    	return $this->belongsToMany(User::class);
    }

    public function brands(){
    	return $this->hasMany(Brand::class);
    }

     public function campaigns(){
    	return $this->belongsTo("App\Campaign");
    }
}
