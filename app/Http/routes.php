<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('/', 'Auth\AuthController@getLogin');

Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
	
// Route::get('/dashboard', ['middleware' => 'auth', function () {
//     return view("dashboard.index");
// }]);

Route::group(["middleware" => "auth"], function(){

Route::get("leads", "LeadController@index");

Route::get("/date1", "LeadController@leadsperday");

Route::get("/date2", "LeadController@leadsviadate");

Route::get("/daily", "LeadController@todaysleads");

Route::get("/daily1","LeadController@todaystotalleads");
Route::get("/monthly", "LeadController@monthlyleads");

Route::get("/monthly1", "LeadController@monthlytotalleads");

Route::get("/weekly", "LeadController@weeklyleads");
Route::get("/weekly1", "LeadController@weeklytotalleads");

Route::get("/export/all","ExportController@exportallleads");
Route::get("/export/csv/all","ExportController@exportcsvleads");


Route::get("/export/daily", "ExportController@todaysreportexport");
Route::get("/export/csv/daily", "ExportController@todaysreportcsv");


Route::get("/export/monthly", "ExportController@monthlyreportexport");
Route::get("/export/csv/monthly", "ExportController@monthlyreportexportcsv");

Route::get("/export/weekly","ExportController@weeklyreportexport");
Route::get("/export/csv/weekly", "ExportController@weeklyreportexportcsv");


Route::get("/filter/leads", "FilterController@filterviathearray");

Route::get("/export/datewise","ExportController@exportleadsperday");
Route::get("/export/csv/datewise","ExportController@exportleadsperdaycsv");

Route::get("/filter/total", "FilterController@filtertotal");

Route::get("/dashboard", "OverviewController@index");

Route::get("/overview/daily", "OverviewController@todaysoverview");


Route::get("/overview/total","OverviewController@totalfilter");

Route::get("/overview/monthly","OverviewController@monthlyoverview");

Route::get("/overview/datewise","OverviewController@datesoverview");
Route::get("/overview/datewisetotal","OverviewController@datewisetotal");

Route::get("/overview/monthlyleadstotal","OverviewController@totalmonthlyfilter");

Route::get("/overview/weekly", "OverviewController@weekwisefilter");
Route::get("/overview/weeklytotalfilter","OverviewController@weeklytotal");

Route::get("/export/filter/datewise", "FilterController@filterdataexport");
Route::get("/export/csv/filter/datewise", "FilterController@filterdataexportcsv");

Route::get("/user/profile", "RolePermController@getProfilepage");

Route::get("/setclient/{id}","ClientController@setclient");
Route::get("/brands","RolePermController@getBrandpage");

Route::get("/brands/products","RolePermController@getProductpage");

Route::get("/setclientlogin", "ClientController@setclientlogin");
Route::get("/setbrandmanagerlogin", "ClientController@setbrandmanagerlogin");

Route::get("/setclientmanager","ClientController@setclientmanager");


});


Route::group(["middleware"=>["auth","admin"]], function(){

Route::post("/storebrand", "RolePermController@storebrand");

Route::get("/admin/roles/permissions","RolePermController@getrolesperm");
Route::post("/admin/assign/roles/permissions", "RolePermController@storeRolesperms");

Route::post("/admin/roles/storerole", "RolePermController@storeRole");
Route::post("/admin/permission/storeperm", "RolePermController@storePermission");

Route::post("/admin/revoke/permission/{id}", "RolePermController@revoke");

Route::post("/remove/user/{id}","UserController@revokeuserrole");
Route::post("/admin/register","UserController@postRegister");
Route::get("/admin/user/info" ,"UserController@getUserpage");
Route::post("/storebrandscampaigns", "RolePermController@storeBrandscampaigns");
Route::post("/storecampaigns" , "RolePermController@storeCampaigns");

Route::get("/admin/brand" ,"UserController@getbrandscampaign");

// Route::get("/getbrandcampaigns", "RolePermController@getbrandcampaigns");
});

Route::group(["middleware" =>["auth","admin_account"]], function(){

Route::get("/admin/user/info" ,"UserController@getUserpage");
Route::post("/admin/register","UserController@postRegister");

});

Route::post("leads", "LeadController@store");

