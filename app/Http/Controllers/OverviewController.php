<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Lead;
use Carbon\Carbon;
use Auth;
use App\Role;
use App\BrandUser;
use App\Brand;

class OverviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Lead $lead, Request $request){
    $this->lead = $lead;
    }


        
    public function index(Request $request)
    {
        if(is_null($request->session()->get("brand_id")) && Auth::user()->hasRole('super_admin')){
        return Redirect::to("/brands");
    }
    elseif(is_null($request->session()->get("brand_id")) && Auth::user()->hasRole('brand_manager')) {
        return Redirect::to("/brands/products");
    }

        $brand = Brand::all();

        
        $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfMonth();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfMonth();
       $query =  $this->lead->where("created_at",">=",$c)->where("created_at","<",$c1);
if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $leads_total = $query->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $leads_total = $query->where("brand_id","=",$request->session()->get("brand_id"))->count();
   }else {
        $leads_total = $query->count();
   }
    //dd($request->session()->get("brand_id"));
   // //echo $leads_total;
//die();


 

        $query1 = DB::table("leads")->select((array(DB::Raw("DATE(leads.created_at) as month"),DB::Raw("COUNT(leads.id) as id"))))
       ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("month")
        ->orderBy("month","ASC");

        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $month_graph = $query1->whereIn("campaign_id",$campaign_id);
}
   }

        if(!is_null($request->session()->get("brand_id"))) {
       $month_graph = $query1->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $month_graph = $query1->get();
   }


        $query2 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as total_leads"),DB::Raw("leads.source as source"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
       ->groupBy("source")
        ->orderBy("count","DESC");


        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $highest_source_leads = $query2->whereIn("campaign_id",$campaign_id);
}
   }


         if(!is_null($request->session()->get("brand_id"))) {
       $highest_source_leads = $query2->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $highest_source_leads = $query2->get();
   }



        $query3 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as total_campaign"),DB::Raw("leads.campaign_name as campaign_name"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("campaign_name")
        ->orderBy("count","DESC");

        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $highest_campaigns_leads = $query3->whereIn("campaign_id",$campaign_id);
}
   }

         if(!is_null($request->session()->get("brand_id"))) {
       $highest_campaigns_leads = $query3->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $highest_campaigns_leads = $query3->get();
   }


        $query4 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.ad as ad"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("ad")
        ->orderBy("count","DESC");
       
if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $top5_ad = $query4->whereIn("campaign_id",$campaign_id);
}
   }

         if(!is_null($request->session()->get("brand_id"))) {
       $top5_ad = $query4->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();
   }else {
        $top5_ad = $query4->take(5)->get();
   }

       
        $query5 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.ad_group as adgroup"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
       ->where("brand_id","=",$request->session()->get("brand_id"))
        ->groupBy("ad_group")
        ->orderBy("count","DESC");

        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $top5_adgroup = $query5->whereIn("campaign_id",$campaign_id);
}
   }

          if(!is_null($request->session()->get("brand_id"))) {
       $top5_adgroup = $query5->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();
   }else {
        $top5_adgroup = $query5->take(5)->get();
   }

      
         $query6 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.keyword as keyword"))))
         ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
       ->where("brand_id","=",$request->session()->get("brand_id"))
        ->groupBy("keyword")
        ->orderBy("count","DESC");

        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $top5_keyword = $query6->whereIn("campaign_id",$campaign_id);
}
   }

          if(!is_null($request->session()->get("brand_id"))) {
       $top5_keyword = $query6->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();
   }else {
        $top5_keyword = $query6->take(5)->get();
   }

         $query7 = Lead::where("created_at",">=",$c)
       ->where("created_at","<",$c1);

        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query7->whereIn("campaign_id",$campaign_id);
}
   }
   
     if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query7->where("brand_id","=",$request->session()->get("brand_id"))->count();
   }else {
        $filter_total_leads = $query7->count();
   }

   if(empty($filter_total_leads)) {
    $photoAreas = array('/images/image1.jpg', '/images/image2.jpg', '/images/image3.jpg');
    $randomNumber = array_rand($photoAreas);
    $randomImage = $photoAreas[$randomNumber];
    return view("dashboard.dailyempty", compact("randomImage"));

}

        return view("overview.overview",compact("d","color","leads_total","month_graph","highest_source_leads","ad_desc_count","top5_ad","top5_adgroup","top5_keyword","highest_campaigns_leads","p"));
    }


    public function todaysoverview(Request $request){

 $query8 =  $this->lead;

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $leads_total = $query8->whereIn("campaign_id",$campaign_id);
}
   }

     if(!is_null($request->session()->get("brand_id"))) {
       $leads_total = $query8->where("brand_id","=",$request->session()->get("brand_id"))->count();
   }else {
        $leads_total = $query8->count();
   }


       $query9 = DB::table("leads")->select((array(DB::Raw("DATE(leads.created_at) as month"),DB::Raw("COUNT(leads.id) as id"))))
       ->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
       ->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
        ->groupBy("month")
        ->orderBy("month","ASC");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $month_graph = $query9->whereIn("campaign_id",$campaign_id);
}
   }

   if(!is_null($request->session()->get("brand_id"))) {
       $month_graph = $query9->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $month_graph = $query9->get();
   }


        $query10 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as total_leads"),DB::Raw("leads.source as source"))))
        ->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
       ->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
        ->groupBy("source")
        ->orderBy("count","DESC");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $highest_source_leads = $query10->whereIn("campaign_id",$campaign_id);
}
   }

        if(!is_null($request->session()->get("brand_id"))) {
       $highest_source_leads = $query10->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $highest_source_leads = $query10->get();
   }
  

        $query11 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as total_campaign"),DB::Raw("leads.campaign_name as campaign_name"))))
        ->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
       ->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
        ->groupBy("campaign_name")
        ->orderBy("count","DESC");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $highest_campaigns_leads = $query11->whereIn("campaign_id",$campaign_id);
}
   }


          if(!is_null($request->session()->get("brand_id"))) {
       $highest_campaigns_leads = $query11->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $highest_campaigns_leads = $query11->get();
   }
  

        $dell = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.ad as ad"))))
        ->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
       ->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
        ->groupBy("ad")
        ->orderBy("count","DESC");
       
if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $top5_ad = $dell->whereIn("campaign_id",$campaign_id);
}
   }

          if(!is_null($request->session()->get("brand_id"))) {
       $top5_ad = $dell->where("brand_id","=",$request->session()->get("brand_id"))->take(5)
        ->get();
   }else {
        $top5_ad= $dell->take(5)->get();
   }

       
        $query12 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.ad_group as adgroup"))))
        ->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
       ->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
        ->groupBy("ad_group")
        ->orderBy("count","DESC");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $top5_adgroup = $query12->whereIn("campaign_id",$campaign_id);
}
   }

  if(!is_null($request->session()->get("brand_id"))) {
       $top5_adgroup = $query12->where("brand_id","=",$request->session()->get("brand_id"))->take(5)
        ->get();
   }else {
        $top5_adgroup = $query12->take(5)->get();
   }

         $query13 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.keyword as keyword"))))
         ->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
       ->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
        ->groupBy("keyword")
        ->orderBy("count","DESC");

        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $top5_keyword = $query13->whereIn("campaign_id",$campaign_id);
}
   }

         if(!is_null($request->session()->get("brand_id"))) {
       $top5_keyword = $query13->where("brand_id","=",$request->session()->get("brand_id"))->take(5)
        ->get();
   }else {
        $top5_keyword= $query13->take(5)->get();
   }

        $query14 = DB::table("leads")->where("created_at",">=",Carbon::today("Asia/Kolkata"))->where("created_at","<",Carbon::tomorrow("Asia/Kolkata"));

     if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $filter_total_leads = $query14->whereIn("campaign_id",$campaign_id);
}
   }

      
           if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query14->where("brand_id","=",$request->session()->get("brand_id"))->count();
   }else {
        $filter_total_leads= $query14->count();
   }

 if(empty($filter_total_leads)) {
    $photoAreas = array('/images/image1.jpg', '/images/image2.jpg', '/images/image3.jpg');
    $randomNumber = array_rand($photoAreas);
    $randomImage = $photoAreas[$randomNumber];
    return view("dashboard.dailyempty", compact("randomImage"));

}

       return view("overview.overviewfilter",compact("month_graph","highest_source_leads","ad_desc_count","top5_ad","top5_adgroup","top5_keyword","highest_campaigns_leads","leads_total","filter_total_leads"));
    }

    public function totalfilter(Request $request){



         $query15 = Lead::where("created_at",">=",Carbon::today("Asia/Kolkata"))
       ->where("created_at","<",Carbon::tomorrow("Asia/Kolkata"));


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $leads_total = $query15->whereIn("campaign_id",$campaign_id);
}
   }

       if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query15->where("brand_id","=",$request->session()->get("brand_id"))->count();
   }else {
        $filter_total_leads = $query15->count();
   }

       return view("overview.totalleadsoverview", compact("filter_total_leads"));

    }

    public function totalmonthlyfilter(Request $request){



  $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfMonth();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfMonth();

    $query16 = Lead::where("created_at",">=",$c)
       ->where("created_at","<",$c1);

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query16->whereIn("campaign_id",$campaign_id);
}
   }

        if(!is_null($request->session()->get("brand_id"))) {
    $filter_total_leads = $query16->where("brand_id","=",$request->session()->get("brand_id"))->count();
   }else {
        $filter_total_leads = $query16->count();
   }


       return view("overview.monthly", compact("filter_total_leads"));

    }

    public function monthlyoverview(Request $request){

 $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfMonth();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfMonth();

 $query180 =  $this->lead;

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $leads_total = $query180->whereIn("campaign_id",$campaign_id);
}

   }

if(!is_null($request->session()->get("brand_id"))) {
    $leads_total = $query180->where("brand_id","=",$request->session()->get("brand_id"))->count();
   }else {
        $leads_total =  $query180->count();
   }

        $query18 = DB::table("leads")->select((array(DB::Raw("DATE(leads.created_at) as month"),DB::Raw("COUNT(leads.id) as id"))))
       ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("month")
        ->orderBy("month","ASC");

        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $month_graph = $query18->whereIn("campaign_id",$campaign_id);
}
   }

                if(!is_null($request->session()->get("brand_id"))) {
    $month_graph = $query18->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $month_graph =  $query18->get();
   }

        $query19 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as total_leads"),DB::Raw("leads.source as source"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("source")
        ->orderBy("count","DESC");

        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $highest_source_leads = $query19->whereIn("campaign_id",$campaign_id);
}
   }

 if(!is_null($request->session()->get("brand_id"))) {
    $highest_source_leads = $query19->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $highest_source_leads =  $query19->get();
   }
  

        $query20 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as total_campaign"),DB::Raw("leads.campaign_name as campaign_name"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("campaign_name")
        ->orderBy("count","DESC");
        
 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $highest_source_leads = $query20->whereIn("campaign_id",$campaign_id);
}
   }

        if(!is_null($request->session()->get("brand_id"))) {
    $highest_campaigns_leads = $query20->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $highest_campaigns_leads =  $query20->get();
   }
  

        $query21 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.ad as ad"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("ad")
        ->orderBy("count","DESC");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $top5_ad = $query21->whereIn("campaign_id",$campaign_id);
}
   }
      
 if(!is_null($request->session()->get("brand_id"))) {
    $top5_ad = $query21->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();

   }else {
        $top5_ad =  $query21->take(5)->get();
   }

       
        $query22 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.ad_group as adgroup"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("ad_group")
        ->orderBy("count","DESC");

    if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $top5_adgroup = $query22->whereIn("campaign_id",$campaign_id);
}
   }
       

         if(!is_null($request->session()->get("brand_id"))) {
    $top5_adgroup = $query22->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();

   }else {
        $top5_adgroup =  $query22->take(5)->get();
   }
  

      
         $query23 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.keyword as keyword"))))
         ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("keyword")
        ->orderBy("count","DESC");

    if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $top5_keyword = $query23->whereIn("campaign_id",$campaign_id);
}
   }

          if(!is_null($request->session()->get("brand_id"))) {
    $top5_keyword = $query23->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();

   }else {
        $top5_keyword =  $query23->take(5)->get();
   }

         $query24 = Lead::where("created_at",">=",$c)
       ->where("created_at","<",$c1);

         if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query24->whereIn("campaign_id",$campaign_id);
}
   }

       
       if(!is_null($request->session()->get("brand_id"))) {
    $filter_total_leads = $query24->where("brand_id","=",$request->session()->get("brand_id"))->count();

   }else {
        $filter_total_leads =  $query24->count();
   }

 if(empty($filter_total_leads)) {
    $photoAreas = array('/images/image1.jpg', '/images/image2.jpg', '/images/image3.jpg');
    $randomNumber = array_rand($photoAreas);
    $randomImage = $photoAreas[$randomNumber];
    return view("dashboard.dailyempty", compact("randomImage"));

}

        return view("overview.monthlyleads",compact("month_graph","highest_source_leads","ad_desc_count","top5_ad","top5_adgroup","top5_keyword","highest_campaigns_leads","leads_total","filter_total_leads"));
    }


    public function weeklytotal(Request $request){


  $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfWeek();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfWeek();


         $query25 = Lead::where("created_at",">=",$c)
       ->where("created_at","<",$c1);

    if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query25->whereIn("campaign_id",$campaign_id);
}
   }

        if(!is_null($request->session()->get("brand_id"))) {
    $filter_total_leads = $query25->where("brand_id","=",$request->session()->get("brand_id"))->count();

   }else {
        $filter_total_leads =  $query25->count();
   }

       return view("overview.monthly", compact("filter_total_leads"));

    }

    public function weekwisefilter(Request $request){

 $leads_total =  $this->lead->count();
    $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfWeek();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfWeek();



 $query50 = Lead::where("created_at",">=",$c)
       ->where("created_at","<",$c1);

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query50->whereIn("campaign_id",$campaign_id);
}
   }


        if(!is_null($request->session()->get("brand_id"))) {
    $filter_total_leads = $query50->where("brand_id","=",$request->session()->get("brand_id"))->count();

   }else {
        $filter_total_leads =  $query50->count();
   }

        $query51 = DB::table("leads")->select((array(DB::Raw("DATE(leads.created_at) as month"),DB::Raw("COUNT(leads.id) as id"))))
       ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("month")
        ->orderBy("month","ASC");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $month_graph = $query51->whereIn("campaign_id",$campaign_id);
}
   }
        if(!is_null($request->session()->get("brand_id"))) {
    $month_graph = $query51->where("brand_id","=",$request->session()->get("brand_id"))->get();

   }else {
        $month_graph =  $query51->get();
   }


        $query27 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as total_leads"),DB::Raw("leads.source as source"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("source")
        ->orderBy("count","DESC");

    if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $highest_source_leads = $query27->whereIn("campaign_id",$campaign_id);
}
   }

        if(!is_null($request->session()->get("brand_id"))) {
    $highest_source_leads = $query27->where("brand_id","=",$request->session()->get("brand_id"))->get();

   }else {
        $highest_source_leads =  $query27->get();
   }

        $query28 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as total_campaign"),DB::Raw("leads.campaign_name as campaign_name"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("campaign_name")
        ->orderBy("count","DESC");

        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $highest_campaigns_leads = $query28->whereIn("campaign_id",$campaign_id);
}
   }

if(!is_null($request->session()->get("brand_id"))) {
    $highest_campaigns_leads = $query28->where("brand_id","=",$request->session()->get("brand_id"))->get();

   }else {
        $highest_campaigns_leads =  $query28->get();
   }


        $query55 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.ad as ad"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("ad")
        ->orderBy("count","DESC");
    
 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $top5_ad = $query55->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
    $top5_ad = $query55->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();

   }else {
        $top5_ad =  $query55->take(5)->get();
   }


        $query29 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.ad_group as adgroup"))))
        ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("ad_group")
        ->orderBy("count","DESC");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $top5_adgroup = $query29->whereIn("campaign_id",$campaign_id);
}
   }

        if(!is_null($request->session()->get("brand_id"))) {
    $top5_adgroup = $query29->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();

   }else {
        $top5_adgroup =  $query29->take(5)->get();
   }
        
         $query30 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.keyword as keyword"))))
         ->where("leads.created_at",">=",$c)
       ->where("leads.created_at","<",$c1)
        ->groupBy("keyword")
        ->orderBy("count","DESC");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $top5_keyword = $query30->whereIn("campaign_id",$campaign_id);
}
   }

if(!is_null($request->session()->get("brand_id"))) {
    $top5_keyword = $query30->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();

   }else {
        $top5_keyword =  $query30->take(5)->get();
   }

 if(empty($filter_total_leads)) {
    $photoAreas = array('/images/image1.jpg', '/images/image2.jpg', '/images/image3.jpg');
    $randomNumber = array_rand($photoAreas);
    $randomImage = $photoAreas[$randomNumber];
    return view("dashboard.dailyempty", compact("randomImage"));

}
       
        return view("overview.monthlyleads",compact("month_graph","highest_source_leads","ad_desc_count","top5_ad","top5_adgroup","top5_keyword","highest_campaigns_leads","leads_total",'filter_total_leads'));


    }

    
    public function datewisetotal(Request $request){

       

            $input1  = $request["daterangepicker_start"];
//var_dump($input1);
    $input2 = $request["daterangepicker_end"];

    if(isset($input1) && isset($input2)){
    \Config::set('app.timezone',  'UTC');
    $startDate  = strtotime($input1);
    $endDate = strtotime($input2);

    \Config::set('app.timezone',  'Asia/Kolkata');
    $startDate = date('Y-m-d H:i:s', $startDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    $endDate = new \Datetime($endDate);
    $endDate  = $endDate->modify("+1 DAY");

$query31 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate);

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query31->whereIn("campaign_id",$campaign_id);
}
   }

if(!is_null($request->session()->get("brand_id"))) {
    $filter_total_leads = $query31->where("brand_id","=",$request->session()->get("brand_id"))->get();

   }else {
        $filter_total_leads =  $query31->get();
   }

return view("overview.datewise", compact("filter_total_leads")); 
}
}


public function datesoverview(Request $request){

    


 $leads_total =  $this->lead->count();
     $input1  = $request["daterangepicker_start"];
//var_dump($input1);
    $input2 = $request["daterangepicker_end"];

    if(isset($input1) && isset($input2)){
    \Config::set('app.timezone',  'UTC');
    $startDate  = strtotime($input1);
    $endDate = strtotime($input2);

    \Config::set('app.timezone',  'Asia/Kolkata');
    $startDate = date('Y-m-d H:i:s', $startDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    $endDate = new \Datetime($endDate);
    $endDate  = $endDate->modify("+1 DAY");
    
    $query32 = DB::table("leads")->select((array(DB::Raw("DATE(leads.created_at) as month"),DB::Raw("COUNT(leads.id) as id"))))
       ->where("leads.created_at",">=",$startDate)
       ->where("leads.created_at","<",$endDate)
        ->groupBy("month")
        ->orderBy("month","ASC");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $month_graph = $query32->whereIn("campaign_id",$campaign_id);
}
   }

if(!is_null($request->session()->get("brand_id"))) {
    $month_graph = $query32->where("brand_id","=",$request->session()->get("brand_id"))->get();

   }else {
        $month_graph =  $query32->get();
   }

        $query33 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as total_leads"),DB::Raw("leads.source as source"))))
        ->where("leads.created_at",">=",$startDate)
       ->where("leads.created_at","<",$endDate)
        ->groupBy("source")
        ->orderBy("count","DESC");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $month_graph = $query33->whereIn("campaign_id",$campaign_id);
}
   }

  if(!is_null($request->session()->get("brand_id"))) {
    $highest_source_leads = $query33->where("brand_id","=",$request->session()->get("brand_id"))->get();

   }else {
        $highest_source_leads =  $query33->get();
   }

        $query34 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as total_campaign"),DB::Raw("leads.campaign_name as campaign_name"))))
        ->where("leads.created_at",">=",$startDate)
       ->where("leads.created_at","<",$endDate)
        ->groupBy("campaign_name")
        ->orderBy("count","DESC");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $highest_campaigns_leads = $query34->whereIn("campaign_id",$campaign_id);
}
   }

    if(!is_null($request->session()->get("brand_id"))) {
    $highest_campaigns_leads = $query34->where("brand_id","=",$request->session()->get("brand_id"))->get();

   }else {
        $highest_campaigns_leads =  $query34->get();
   }


        $query35 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.ad as ad"))))
        ->where("leads.created_at",">=",$startDate)
       ->where("leads.created_at","<",$endDate)
        ->groupBy("ad")
        ->orderBy("count","DESC");
        


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $top5_ad = $query35->whereIn("campaign_id",$campaign_id);
}
   }

        if(!is_null($request->session()->get("brand_id"))) {
    $top5_ad = $query35->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();

   }else {
        $top5_ad =  $query35->take(5)->get();
   }

       
        $query36 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.ad_group as adgroup"))))
        ->where("leads.created_at",">=",$startDate)
       ->where("leads.created_at","<",$endDate)
        ->groupBy("ad_group")
        ->orderBy("count","DESC");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $top5_adgroup = $query36->whereIn("campaign_id",$campaign_id);
}
   }

          if(!is_null($request->session()->get("brand_id"))) {
    $top5_adgroup = $query36->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();

   }else {
        $top5_adgroup =  $query36->take(5)->get();
   }

       
$query37 = DB::table("leads")->select((array(DB::Raw("count(*) as count"),DB::Raw("COUNT(leads.id) as id"),DB::Raw("leads.keyword as keyword"))))
         ->where("leads.created_at",">=",$startDate)
       ->where("leads.created_at","<",$endDate)
        ->groupBy("keyword")
        ->orderBy("count","DESC");
       

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $top5_keyword = $query37->whereIn("campaign_id",$campaign_id);
}
   }
        
            if(!is_null($request->session()->get("brand_id"))) {
    $top5_keyword = $query37->where("brand_id","=",$request->session()->get("brand_id"))->take(5)->get();

   }else {
        $top5_keyword =  $query37->take(5)->get();
   }

       $query38 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate);

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));

   if(count($campaign_id) > 0){
       $filter_total_leads = $query38->whereIn("campaign_id",$campaign_id);
}
   }
        
            if(!is_null($request->session()->get("brand_id"))) {
    $filter_total_leads = $query38->where("brand_id","=",$request->session()->get("brand_id"))->get();
    //dd($filter_total_leads);
   }else {
        $filter_total_leads =  $query38->get();
   }

if(empty($month_graph)) {
    $photoAreas = array('/images/image1.jpg', '/images/image2.jpg', '/images/image3.jpg');
    $randomNumber = array_rand($photoAreas);
    $randomImage = $photoAreas[$randomNumber];
    return view("dashboard.dailyempty", compact("randomImage"));

}

return view("overview.datewiseleads",compact("month_graph","highest_source_leads","ad_desc_count","top5_ad","top5_adgroup","top5_keyword","highest_campaigns_leads","leads_total","filter_total_leads"));


}

}
}
