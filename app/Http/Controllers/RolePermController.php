<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use App\User;
use Auth;
use App\BrandUser;
use Session;
use App\Campaign;
use App\Brand;
use App\Allcampaign;
use DB;
class RolePermController extends Controller
{
	public function __construct(){
		$this->middleware("auth");
		//$this->middleware("admin");
	}

	public function getBrandpage(Request $request){
		if(Session::has('brand_id')){
			//dd(Session::get("brand_id"));
		$brandu = Campaign::where("brand_id","=" ,$request->session()->get("brand_id"))->get();
	}
		// dd($brandu);
		return view("admin.brands", compact("brandu"));
	}
	public function getProductpage(Request $request){
	
	$query = BrandUser::where("user_id","=",Auth::user()->id);
	
	if(session()->has("brand")){
		$product = $query->where("brand_id","=",$request->session()->get("brand_id"))->first();
            dd($product);
	}
	else{
		$product = $query->first();
	}
		$campaign = explode(",",$product->campaign_id);
	
	//dd($campaign);
		$q = Campaign::whereIn("id",$campaign)->get();
		//dd($product->campaign_id);
	
	return view("admin.products", compact("product","q"));
	}

    public function storeRole(Request $request){
    	// $this->validate($request,[
    	// 	"name" =>"required|unique:roles|max:50|alpha",
    	// 	"label"=>"required|unique:roles|max:50|alpha"
    	// 	]);
    	$addrole = new Role;
    	$addrole->name = $request["name"];
    	$addrole->label = $request["label"];
    	
    	$addrole->addRole();

    	return Redirect::back();
    }

   public function storebrand(Request $request){
   		$this->validate($request,[
    		"name" =>"required|unique:brands|max:50",
    		"label"=>"required|unique:brands|max:50"
    		]);

   		$brand = new Brand;
    	$brand->name = $request["name"];
    	$brand->label = $request["label"];
    	$brand->save();

   }

   public function storePermission(Request $request){
    	// $this->validate($request,[
    	// 	"name" =>"required|unique:permissions|max:50|alpha",
    	// 	"label"=>"required|unique:permissions|max:50|alpha"
    	// 	]);
    	$addperm = new Permission;
    	$addperm->name = $request["name"];
    	$addperm->label = $request["label"];
    	$addperm->save();
    }


    public function getrolesperm(){
    	$roles = Role::all();
    	$perm = Permission::all();

    	return view("admin.rolesperms", compact("roles","perm"));
    }

     public function storeCampaigns(Request $request){
    	$this->validate($request,[
    		"name" =>"required|unique:allcampaigns",
    		"label"=>"required|unique:allcampaigns"
    		]);

    	$campaigns = new Allcampaign;
    	$campaigns->name = $request["name"];
    	$campaigns->label = $request["label"];
    	$campaigns->save();
    }
    public function getbrandcampaigns(Request $request){
    	$brand_id = $request["brand_id"]; //this comes as a id

    	$campaign_object = Campaign::where("brand_id", '=', $brand_id)->get();
    	
    	return view("admin.user", compact("campaign_object"));
    }

    public function storeBrandscampaigns(Request $request){

    	$this->validate($request, [
    		"brand_id" => "required|integer",
    		"campaign_name" =>"required|unique:campaigns"
    		]);

    	$brand_id = $request["brand_id"];
    	//dd($brand_id);
    	$campaign_name = $request["campaign_name"]; //comes back as a array as a array
    	dd($campaign_name);
    	if(isset($brand_id) && isset($campaign_name)){
    		$brand_object = Brand::where("id",$brand_id)->firstOrFail();

    		foreach($campaign_name as $name){
    			  $campaign_object = Campaign::where('campaign_name', $name)->firstOrFail();   

    			  $brand_object->attach($campaign_object);
    		}
    		$brand_object->save();

    	}

    	return Redirect::back();

    }


     public function storeRolesperms(Request $request){
       $role_id = $request["role_id"];
$permission_ids = $request["permission_id"];

//dd($permission_ids);
 // This comes back as an array.
if(isset($permission_ids) && isset($role_id)){
	
    $role_object = Role::where('id', $role_id)->firstOrFail();
    foreach ($permission_ids as $permission_id) {
        // loop through each permission id
        // find the corresponding permission object in the database
        
        $permission_object = Permission::where('id', $permission_id)->firstOrFail();        
        // assign the object to the role
        $role_object->givePermissionTo($permission_object);
    
}
    $role_object->save();
}
return Redirect::back();
}

public function revoke(Request $request, $id){
         
$permission_ids = $request->perms; // This comes back as an array.
//dd($permission_ids);
    $role_object = Role::find($id);
  // dd($role_object);
        // loop through each permission id
        // find the corresponding permission object in the database

       foreach ($permission_ids as $permission_id) {
        // loop through each permission id
        // find the corresponding permission object in the database
        $permission_object = Permission::where('id', $permission_id)->firstOrFail();

        // assign the object to the role
        $role_object->revokeTo($permission_object);
    }

    return Redirect::back();
}



public function getProfilepage(){

	$user = User::where("id","=",Auth::id())->first();
	$brands = Brand::all();
	// $brand = BrandUser::where("user_id",'=',Auth::id())->first();
	// dd($brand->brands()->name);
	// 
	//dd($user->brands());
	return view("admin.userprofile", compact("user","brands"));
}
}
