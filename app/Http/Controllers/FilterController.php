<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use App\Lead;
use App\Role;
use App\BrandUser;


class FilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filterviathearray(Request $request)
    {

    $filter = $request["filter"];
    $condition = $request["condition"];
    $having = $request["having"];
    $inputdate1 = $request->input("daterangepicker_start2");
    $inputdate2 = $request->input("daterangepicker_end2");
  
    if(empty($filter) || empty($condition) || empty($having)){

        return view("filters.empty");
    }

elseif(isset($filter) && isset($condition) && isset($having) && isset($inputdate1) && isset($inputdate2)){
         \Config::set('app.timezone',  'UTC');
    $startDate  = strtotime($inputdate1);
    $endDate = strtotime($inputdate2);

    \Config::set('app.timezone',  'Asia/Kolkata');
    $startDate = date('Y-m-d H:i:s', $startDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    $endDate = new \Datetime($endDate);
    $endDate  = $endDate->modify("+1 DAY");

         if($condition === "contain" && $filter === "city"){
             $query = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query->get();
   }

$query1 =  DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query1->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query1->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query1->get();
   }

        return view("filters.datewise", compact("filter_leads", "filter_graph"));

         }

elseif($condition === "doesnotcontain" && $filter === "city"){

        $query2 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query2->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query2->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query2->get();
   }

$query3  =DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query3->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query3->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query3->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }

        elseif($condition === "is" && $filter === "city"){

        $query4 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query4->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query4->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query4->get();
   }


       $query5 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query5->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query5->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query5->get();
   }


        return view("filters.all", compact("filter_leads","filter_graph"));

    }

        elseif($condition === "startwith" && $filter === "city"){

        $query6 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query6->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query6->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query6->get();
   }

$query7 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query7->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query7->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query7->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }
        elseif($condition === "endwith" && $filter === "city"){

        $query8 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query8->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query8->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query8->get();
   }

 $query9 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query9->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query9->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query9->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }

elseif($condition === "contain" && $filter === "source"){

        $query10 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query10->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query10->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query10->get();
   }

  $query11 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query11->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query11->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query11->get();
   }


        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "doesnotcontain" && $filter === "source"){

        $query12 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query12->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query12->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query12->get();
   }

 $query13 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query13->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query13->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query13->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "is" && $filter === "source"){

        $query14 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query14->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query14->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query14->get();
   }

 $query15 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query15->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query15->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query15->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

    elseif($condition === "startwith" && $filter === "source"){

        $query16 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query16->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query16->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query16->get();
   }

 $query17 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query17->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query17->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query17->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "endwith" && $filter === "source"){

        $query18 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query18->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query18->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query18->get();
   }

$query19 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query19->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query19->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query19->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }

//campaign
elseif($condition === "contain" && $filter === "campaign"){

        $query20 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query20->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query20->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query20->get();
   }

$query21 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query21->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query21->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query21->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "doesnotcontain" && $filter === "campaign"){

        $query22 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query22->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query22->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query22->get();
   }

 $query23 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query23->whereIn("campaign_id",$campaign_id);
}
   }

if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query23->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query23->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "is" && $filter === "campaign"){

        $query24 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query24->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query24->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query24->get();
   }

$query25 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query25->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query25->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query25->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

    elseif($condition === "startwith" && $filter === "campaign"){

        $query26 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query26->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query26->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query26->get();
   }

$query27 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");
if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query27->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query27->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query27->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "endwith" && $filter === "campaign"){

        $query28 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query28->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query28->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query28->get();
   }

 $query29 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query29->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query29->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query29->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

//ad group  

elseif($condition === "contain" && $filter === "adgroup"){

        $query30 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query30->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query30->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query30->get();
   }

 $query31 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query31->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query31->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query31->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "doesnotcontain" && $filter === "adgroup"){

        $query32 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query32->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query32->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query32->get();
   }

$query33 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");
if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query33->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query33->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query33->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "is" && $filter === "adgroup"){

        $query34 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query34->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query34->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query34->get();
   }


 $query35 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query35->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query35->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query35->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }

    elseif($condition === "startwith" && $filter === "adgroup"){

        $query36 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query36->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query36->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query36->get();
   }

  $query37 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query37->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query37->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query37->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "endwith" && $filter === "adgroup"){

        $query38 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query38->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query38->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query38->get();
   }


  $query39 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query39->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query39->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query39->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

//keyword

elseif($condition === "contain" && $filter === "keyword"){

        $query40 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query40->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query40->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query40->get();
   }


$query41 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query41->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query41->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query41->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "doesnotcontain" && $filter === "keyword"){

        $query42 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query42->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query42->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query42->get();
   }


  $query43 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query43->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query43->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query43->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "is" && $filter === "keyword"){

        $query44 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query6->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query44->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query44->get();
   }

  $query45 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query45->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query45->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query45->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

    elseif($condition === "startwith" && $filter === "keyword"){

        $query48 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query48->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query48->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query48->get();
   }

$query49 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query49->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query49->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query49->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "endwith" && $filter === "keyword"){

        $query50 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query50->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query50->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query50->get();
   }

 $query51 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query51->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query51->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query51->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }

}

elseif(isset($filter) && isset($condition) && isset($having) && empty($inputdate1) && empty($inputdate2)){
        
        if($condition === "contain" && $filter === "city"){
        $query52 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query52->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query52->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query52->get();
   }

$query53 =  DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query53->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query53->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query53->get();
   }

        return view("filters.all", compact("filter_leads", "filter_graph"));
    }

    elseif($condition === "doesnotcontain" && $filter === "city"){

        $query54 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query54->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query54->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query54->get();
   }

$query55  =DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query55->whereIn("campaign_id",$campaign_id);
}
   }

if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query55->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query55->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

        elseif($condition === "is" && $filter === "city"){

        $query56 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query56->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query56->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query56->get();
   }

       $query57 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query57->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query57->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query57->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

        elseif($condition === "startwith" && $filter === "city"){

        $query58 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query58->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query58->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query58->get();
   }

$query59 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query59->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query59->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query59->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
        elseif($condition === "endwith" && $filter === "city"){

        $query60 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query60->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query60->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query60->get();
   }

 $query61 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query61->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query61->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query61->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }

elseif($condition === "contain" && $filter === "source"){

        $query62 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query62->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query62->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query62->get();
   }

  $query63 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query63->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query63->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query63->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "doesnotcontain" && $filter === "source"){

        $query64 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query64->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query64->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query64->get();
   }

 $query65 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");
if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query65->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query65->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query65->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }

    elseif($condition === "is" && $filter === "source"){

        $query66 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query66->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query66->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query66->get();
   }


 $query67 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query67->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query67->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query67->get();
   }


        return view("filters.all", compact("filter_leads","filter_graph"));

    }

    elseif($condition === "startwith" && $filter === "source"){

        $query68 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query68->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query68->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query68->get();
   }


 $query69 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query69->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query69->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query69->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "endwith" && $filter === "source"){

        $query70 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query70->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query70->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query70->get();
   }

$query71 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

  if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query71->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query71->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query71->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

//campaign
elseif($condition === "contain" && $filter === "campaign"){

        $query72 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query72->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query72->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query72->get();
   }

$query73 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query73->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query73->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query73->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "doesnotcontain" && $filter === "campaign"){

        $query74 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query74->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query74->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query74->get();
   }

 $query75 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query75->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query75->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query75->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "is" && $filter === "campaign"){

        $query76 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query76->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query76->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query76->get();
   }

$query77 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query77->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query77->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query77->get();
   }


        return view("filters.all", compact("filter_leads","filter_graph"));

    }

    elseif($condition === "startwith" && $filter === "campaign"){

        $query78 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query78->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query78->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query78->get();
   }

$query79 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query79->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query79->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query79->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "endwith" && $filter === "campaign"){

        $query80 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query80->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query80->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query80->get();
   }

 $query81 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query81->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query81->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query81->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

//ad group  

elseif($condition === "contain" && $filter === "adgroup"){

        $query82 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query82->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query82->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query82->get();
   }

 $query83 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query83->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query83->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query83->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "doesnotcontain" && $filter === "adgroup"){

        $query84 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query84->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query84->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query84->get();
   }


$query85 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query85->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query85->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query85->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "is" && $filter === "adgroup"){

        $query86 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query86->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query86->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query86->get();
   }


 $query87 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query87->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query87->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query87->get();
   }


        return view("filters.all", compact("filter_leads","filter_graph"));

    }

    elseif($condition === "startwith" && $filter === "adgroup"){

        $query88 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query88->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query88->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query88->get();
   }

  $query89 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having%")
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query89->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query89->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query89->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "endwith" && $filter === "adgroup"){

        $query90 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query90->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query90->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query90->get();
   }

  $query91 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");


 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query91->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query91->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query91->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

//keyword

elseif($condition === "contain" && $filter === "keyword"){

        $query92 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query92->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query92->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query92->get();
   }

$query93 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

 if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query93->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query93->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query93->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "doesnotcontain" && $filter === "keyword"){

        $query94 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query94->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query94->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query94->get();
   }

  $query95 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query95->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query95->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query95->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "is" && $filter === "keyword"){

        $query96 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query96->whereIn("campaign_id",$campaign_id);
}
   }

if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query96->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query96->get();
   }

  $query97 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query97->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query97->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query97->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }

    elseif($condition === "startwith" && $filter === "keyword"){

        $query98 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query98->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query98->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query98->get();
   }

$query99 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_graph = $query99->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_graph = $query99->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_graph = $query99->get();
   }
        return view("filters.all", compact("filter_leads","filter_graph"));

    }
    elseif($condition === "endwith" && $filter === "keyword"){

        $query100 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query100->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query100->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query100->get();
   }

 $query101 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");
if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $query101->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $query101->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $query101->get();
   }

        return view("filters.all", compact("filter_leads","filter_graph"));

    }


}
    }


    public function filtertotal(Request $request){
    
    $filter = $request["filter"];
    $condition = $request["condition"];
    $having = $request["having"];
    $inputdate1 = $request["daterangepicker_start2"];
    $inputdate2 = $request["daterangepicker_end2"];
if(isset($filter) && isset($condition) && isset($having) && isset($inputdate1)
        && isset($inputdate2)){
    \Config::set('app.timezone',  'UTC');
    $startDate  = strtotime($inputdate1);
    $endDate = strtotime($inputdate2);

    \Config::set('app.timezone',  'Asia/Kolkata');
    $startDate = date('Y-m-d H:i:s', $startDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    $endDate = new \Datetime($endDate);
    $endDate  = $endDate->modify("+1 DAY");

     if($condition === "contain" && $filter === "city"){
            
 $query102 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("city","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query102->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query102->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query102->get();
   }
        return view("filters.totalleads", compact("filter_total_leads"));
    }

    elseif($condition === "doesnotcontain" && $filter === "city"){
 $query103 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("city","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query103->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query103->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query103->get();
   }

        return view("filters.totalleads", compact("filter_total_leads"));

    }

        elseif($condition === "is" && $filter === "city"){

 $query104 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("city","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query104->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query104->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query104->get();
   }


return view("filters.totalleads", compact("filter_total_leads"));

    }

        elseif($condition === "startwith" && $filter === "city"){

 $query105 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("city","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query105->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query105->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query105->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
        elseif($condition === "endwith" && $filter === "city"){

         
 $query106 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("city","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query106->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query106->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query106->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

elseif($condition === "contain" && $filter === "source"){
           
 $query107 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("source","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query107->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query107->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query107->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));


    }
    elseif($condition === "doesnotcontain" && $filter === "source"){

      $query108 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("source","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query108->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query108->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query108->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "is" && $filter === "source"){

$query109 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("source","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query109->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query109->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query109->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

    elseif($condition === "startwith" && $filter === "source"){

$query110 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("source","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query110->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query110->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query110->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "endwith" && $filter === "source"){

$query111 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("source","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query111->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query111->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query111->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));
    }

//campaign
elseif($condition === "contain" && $filter === "campaign"){

$query112 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("campaign_name","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query112->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query112->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query112->get();
   }

        return view("filters.totalleads", compact("filter_total_leads"));
   
    }
    elseif($condition === "doesnotcontain" && $filter === "campaign"){

       $query113 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("campaign_name","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query113->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query113->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query113->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "is" && $filter === "campaign"){
 
       $query114 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("campaign_name","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query114->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query114->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query114->get();
   }
        return view("filters.totalleads", compact("filter_total_leads"));

    }

    elseif($condition === "startwith" && $filter === "campaign"){

        
       $query115 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("campaign_name","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query115->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query115->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query115->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "endwith" && $filter === "campaign"){

 
       $query116 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("campaign_name","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query116->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query116->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query116->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

//ad group  

elseif($condition === "contain" && $filter === "adgroup"){

      
$query117 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("ad_group","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query117->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query117->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query117->get();
   }

        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "doesnotcontain" && $filter === "adgroup"){

     
        $query118 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("ad_group","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query118->whereIn("campaign_id",$campaign_id);
}
   }

if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query118->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query118->get();
   }

        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "is" && $filter === "adgroup"){

   $query119 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("ad_group","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query119->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query119->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query119->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

    elseif($condition === "startwith" && $filter === "adgroup"){

    
        $query120 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("ad_group","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query120->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query120->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query120->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "endwith" && $filter === "adgroup"){

  
         $query121 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("ad_group","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query121->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query121->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query121->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

//keyword

elseif($condition === "contain" && $filter === "keyword"){

 
         $query122 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("keyword","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query122->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query122->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query122->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "doesnotcontain" && $filter === "keyword"){

      

         $query123 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("keyword","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query123->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query123->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query123->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));
    }
    elseif($condition === "is" && $filter === "keyword"){

  
       $query124 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("keyword","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query124->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query124->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query124->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

    elseif($condition === "startwith" && $filter === "keyword"){

  
       $query125 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("keyword","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query125->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query125->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query125->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "endwith" && $filter === "keyword"){

      $query126 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("keyword","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query126->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query126->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query126->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

}
elseif(isset($filter) && isset($condition) && isset($having) && empty($inputdate1)
        && empty($inputdate2)){
        if($condition === "contain" && $filter === "city"){
            
 $query127 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("city","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query127->whereIn("campaign_id",$campaign_id);
}
   }

if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query127->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query127->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));
    }

    elseif($condition === "doesnotcontain" && $filter === "city"){

         
 $query128 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("city","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query128->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query128->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query128->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

        elseif($condition === "is" && $filter === "city"){

 $query129 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("city","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query129->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query129->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query129->get();
   }


return view("filters.totalleads", compact("filter_total_leads"));

    }

        elseif($condition === "startwith" && $filter === "city"){

 $query130 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("city","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query130->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query130->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query130->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
        elseif($condition === "endwith" && $filter === "city"){

         
 $query131 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("city","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query131->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query131->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query131->get();
   }

        return view("filters.totalleads", compact("filter_total_leads"));

    }

elseif($condition === "contain" && $filter === "source"){

  
 $query132 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("source","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query132->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query132->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query132->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));


    }
    elseif($condition === "doesnotcontain" && $filter === "source"){

      $query133 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("source","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query133->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query133->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query133->get();
   }

        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "is" && $filter === "source"){

$query134 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("source","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query134->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query134->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query134->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

    elseif($condition === "startwith" && $filter === "source"){

       
       $query135 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("source","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query135->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query135->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query135->get();
   }

        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "endwith" && $filter === "source"){

$query136 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("source","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query136->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query136->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query136->get();
   }
        return view("filters.totalleads", compact("filter_total_leads"));
    }

//campaign
elseif($condition === "contain" && $filter === "campaign"){

$query137 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("campaign_name","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query137->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query137->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query137->get();
   }

        return view("filters.totalleads", compact("filter_total_leads"));
   
    }
    elseif($condition === "doesnotcontain" && $filter === "campaign"){

       $query138 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("campaign_name","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query138->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query138->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query138->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "is" && $filter === "campaign"){

 
       $query139 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("campaign_name","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query139->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query139->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query139->get();
   }
        return view("filters.totalleads", compact("filter_total_leads"));

    }

    elseif($condition === "startwith" && $filter === "campaign"){

        
       $query140 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("campaign_name","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query140->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query140->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query140->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "endwith" && $filter === "campaign"){

 
       $query141 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("campaign_name","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query141->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query141->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query141->get();
   }
        return view("filters.totalleads", compact("filter_total_leads"));

    }

//ad group  

elseif($condition === "contain" && $filter === "adgroup"){

      
$query142 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("ad_group","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query142->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query142->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query142->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "doesnotcontain" && $filter === "adgroup"){

     
        $query143 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("ad_group","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query143->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query143->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query143->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "is" && $filter === "adgroup"){

   $query144 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("ad_group","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query144->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query144->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query144->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

    elseif($condition === "startwith" && $filter === "adgroup"){

    
        $query145 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("ad_group","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query145->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query145->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query145->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "endwith" && $filter === "adgroup"){

  
         $query146 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("ad_group","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query146->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query146->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query146->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

//keyword

elseif($condition === "contain" && $filter === "keyword"){

 
         $query147 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("keyword","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query147->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query147->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query147->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "doesnotcontain" && $filter === "keyword"){

      
$query148 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("keyword","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query148->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query148->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query148->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));
    }
    elseif($condition === "is" && $filter === "keyword"){

  
       $query149 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("keyword","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query149->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query149->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query149->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }

    elseif($condition === "startwith" && $filter === "keyword"){

  
       $query150 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("keyword","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query150->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query150->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query150->get();
   }


        return view("filters.totalleads", compact("filter_total_leads"));

    }
    elseif($condition === "endwith" && $filter === "keyword"){

      $query151 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("keyword","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_total_leads = $query151->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_total_leads = $query151->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_total_leads = $query151->get();
   }
        return view("filters.totalleads", compact("filter_total_leads"));

    }
    }
}


public function filterdataexport(Request $request){

 
    $filter = $request["filter_val"];
    $condition = $request["condition_val"];
    $having = $request["having_val"];
    $inputdate1 = $request->input("daterangepicker_start2");
    $inputdate2 = $request->input("daterangepicker_end2");

    if(empty($inputdate1) && empty($inputdate2) && isset($filter) && isset($condition) && isset($having)){
         
    if($condition === "contain" && $filter === "city"){
      
        $q1 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q1->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q1->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q1->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();
    }

    elseif($condition === "doesnotcontain" && $filter === "city"){

        $q2 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q2->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q2->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q2->get();
   }
       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');


    }

        elseif($condition === "is" && $filter === "city"){

        $q3 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q3->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q3->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q3->get();
   }

  
       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

        elseif($condition === "startwith" && $filter === "city"){

        $q4 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q4->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q4->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q4->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
        elseif($condition === "endwith" && $filter === "city"){

        $q5 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q5->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q5->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q5->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();
    }

elseif($condition === "contain" && $filter === "source"){

        $q6 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q6->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q6->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q6->get();
   }

  $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "source"){

        $q7 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q7->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q7->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q7->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "source"){

        $q8 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q8->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q8->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q8->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();
    }

    elseif($condition === "startwith" && $filter === "source"){

        $q9 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q9->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q9->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q9->get();
   }


       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "source"){

        $q10 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q10->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q10->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q10->get();
   }


      $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

//campaign
elseif($condition === "contain" && $filter === "campaign"){

        $q11 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q11->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q11->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q11->get();
   }

    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "campaign"){

        $q12 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q12->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q12->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q12->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();
    }
    elseif($condition === "is" && $filter === "campaign"){

        $q13 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q13->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q13->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q13->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "campaign"){

        $q14 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q14->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q14->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q14->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "campaign"){

        $q16 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q16->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q16->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q16->get();
   }


     $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

//ad group  

elseif($condition === "contain" && $filter === "adgroup"){

        $q17 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q17->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q17->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q17->get();
   }
$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "adgroup"){

        $q18 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q18->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q18->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q18->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "adgroup"){

        $q20 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q20->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q20->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q20->get();
   }

    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "adgroup"){

        $q21 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q21->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q21->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q21->get();
   }


    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "adgroup"){

        $q22 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q22->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q22->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q22->get();
   }

 $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

//keyword

elseif($condition === "contain" && $filter === "keyword"){

        $q23 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q23->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q23->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q23->get();
   }


   $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "keyword"){

        $q24 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q24->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q24->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q24->get();
   }

      $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "keyword"){

        $q25 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q25->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q25->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q25->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "keyword"){

        $q26 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q26->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q26->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q26->get();
   }

   $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "keyword"){

        $q27 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q27->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q27->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q27->get();
   }

 $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    }


    elseif(isset($inputdate1) && isset($inputdate2) && isset($filter) && isset($condition) && isset($having)){

 \Config::set('app.timezone',  'UTC');
    $startDate  = strtotime($inputdate1);
    $endDate = strtotime($inputdate2);

    \Config::set('app.timezone',  'Asia/Kolkata');
    $startDate = date('Y-m-d H:i:s', $startDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    $endDate = new \Datetime($endDate);
    $endDate  = $endDate->modify("+1 DAY");

if($condition === "contain" && $filter === "city"){
      
        $q31 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q31->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q31->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q31->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();
    }

    elseif($condition === "doesnotcontain" && $filter === "city"){

        $q32 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q32->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q32->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q32->get();
   }


       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

        elseif($condition === "is" && $filter === "city"){

        $q33 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q33->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q33->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q33->get();
   }
  
       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

        elseif($condition === "startwith" && $filter === "city"){

        $q34 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q34->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q34->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q34->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
        elseif($condition === "endwith" && $filter === "city"){

        $q35 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q35->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q35->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q35->get();
   }


       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();
    }

elseif($condition === "contain" && $filter === "source"){

        $q36 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q36->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q36->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q36->get();
   }

  $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();
    }
    elseif($condition === "doesnotcontain" && $filter === "source"){

        $q37 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q37->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q37->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q37->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "source"){

        $q38 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q38->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q38->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q38->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();
    }

    elseif($condition === "startwith" && $filter === "source"){

        $q39 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q39->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q39->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q39->get();
   }


       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "source"){

        $q41 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q41->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q41->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q41->get();
   }


      $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

//campaign
elseif($condition === "contain" && $filter === "campaign"){

        $q42 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q42->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q42->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q42->get();
   }

    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "campaign"){

        $q43 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q43->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q43->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q43->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();
    }
    elseif($condition === "is" && $filter === "campaign"){

        $q44 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q44->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q44->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q44->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "campaign"){

        $q46 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q46->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q46->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q46->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "campaign"){

        $q47 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q47->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q47->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q47->get();
   }


     $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

//ad group  

elseif($condition === "contain" && $filter === "adgroup"){

        $q48 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q48->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q48->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q48->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "adgroup"){

        $q49 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q49->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q49->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q49->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "adgroup"){

        $q50 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q50->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q50->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q50->get();
   }


    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "adgroup"){

        $q51 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q51->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q51->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q51->get();
   }

    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "adgroup"){

        $q52 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q52->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q52->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q52->get();
   }

 $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

//keyword

elseif($condition === "contain" && $filter === "keyword"){

        $q53 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q53->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q53->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q53->get();
   }


   $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "keyword"){

        $q54 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q54->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q54->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q54->get();
   }

      $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "keyword"){

        $q55 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q55->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q55->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q55->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "keyword"){

        $q56 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q56->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q56->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q56->get();
   }


   $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "keyword"){

        $q57 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q57->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q57->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q57->get();
   }

 $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();

    }

    }



}


public function filterdataexportcsv(Request $request){

 
    $filter = $request["filter_val"];
    $condition = $request["condition_val"];
    $having = $request["having_val"];
    $inputdate1 = $request->input("daterangepicker_start2");
    $inputdate2 = $request->input("daterangepicker_end2");

    if(empty($inputdate1) && empty($inputdate2) && isset($filter) && isset($condition) && isset($having)){
         
    if($condition === "contain" && $filter === "city"){
      
        $q58 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q58->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q58->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q58->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();
    }

    elseif($condition === "doesnotcontain" && $filter === "city"){

        $q59 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q59->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q59->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q59->get();
   }


       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');


    }

        elseif($condition === "is" && $filter === "city"){

        $q60 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q60->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q60->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q60->get();
   }

  
       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

        elseif($condition === "startwith" && $filter === "city"){

        $q61 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q61->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q61->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q61->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
        elseif($condition === "endwith" && $filter === "city"){

        $q62 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q62->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q62->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q62->get();
   }


       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();
    }

elseif($condition === "contain" && $filter === "source"){

        $filter_leads = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q63->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q63->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q63->get();
   }
  $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "source"){

        $q64 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q64->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q64->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q64->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "source"){

        $q65 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q65->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q65->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q65->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();
    }

    elseif($condition === "startwith" && $filter === "source"){

        $q66 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q66->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q66->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q66->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "source"){

        $q67 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q67->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q67->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q67->get();
   }


      $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

//campaign
elseif($condition === "contain" && $filter === "campaign"){

        $q68 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q68->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q68->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q68->get();
   }

    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "campaign"){

        $q69 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q69->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q69->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q69->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();
    }
    elseif($condition === "is" && $filter === "campaign"){

        $q70 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q70->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q70->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q70->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "campaign"){

        $q71 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q71->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q71->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q71->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "campaign"){

        $q74 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q74->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q74->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q74->get();
   }


     $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

//ad group  

elseif($condition === "contain" && $filter === "adgroup"){

        $q76 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q76->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q76->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q76->get();
   }
$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "adgroup"){

        $q77 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q77->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q77->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q77->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "adgroup"){

        $q80 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q80->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q80->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q80->get();
   }


    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "adgroup"){

        $q81 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q81->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q81->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q81->get();
   }



    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "adgroup"){

        $q82 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q82->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q82->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q82->get();
   }

 $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

//keyword

elseif($condition === "contain" && $filter === "keyword"){

        $q83 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q83->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q83->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q83->get();
   }


   $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "keyword"){

        $q84 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","not like","%$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q84->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q84->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q84->get();
   }

      $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "keyword"){

        $q85 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q85->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q85->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q85->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "keyword"){

        $q86 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having%")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q86->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q86->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q86->get();
   }


   $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "keyword"){

        $q87 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having")
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q87->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q87->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q87->get();
   }

 $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    }


    elseif(isset($inputdate1) && isset($inputdate2) && isset($filter) && isset($condition) && isset($having)){

 \Config::set('app.timezone',  'UTC');
    $startDate  = strtotime($inputdate1);
    $endDate = strtotime($inputdate2);

    \Config::set('app.timezone',  'Asia/Kolkata');
    $startDate = date('Y-m-d H:i:s', $startDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    $endDate = new \Datetime($endDate);
    $endDate  = $endDate->modify("+1 DAY");

if($condition === "contain" && $filter === "city"){
      
        $q88 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q88->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q88->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q88->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();
    }

    elseif($condition === "doesnotcontain" && $filter === "city"){

        $q90 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q90->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q90->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q90->get();
   }


       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

        elseif($condition === "is" && $filter === "city"){

        $q91 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q91->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q91->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q91->get();
   }

  
       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

        elseif($condition === "startwith" && $filter === "city"){

        $q92 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q92->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q92->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q92->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
        elseif($condition === "endwith" && $filter === "city"){

        $q93 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("city","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q93->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q93->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q93->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();
    }

elseif($condition === "contain" && $filter === "source"){

        $q95 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q95->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q95->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q95->get();
   }

  $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "source"){

        $q96 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q96->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q96->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q96->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "source"){

        $q97 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q97->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q97->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q97->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();
    }

    elseif($condition === "startwith" && $filter === "source"){

        $q99 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q99->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q99->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q99->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "source"){

        $q100 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("source","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q100->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q100->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q100->get();
   }
      $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

//campaign
elseif($condition === "contain" && $filter === "campaign"){

        $q101 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q101->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q101->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q101->get();
   }

    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "campaign"){

        $q102 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q102->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q102->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q102->get();
   }

       $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();
    }
    elseif($condition === "is" && $filter === "campaign"){

        $q103 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q103->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q103->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q103->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "campaign"){

        $q105 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q105->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q105->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q105->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "campaign"){

        $q107 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("campaign_name","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q107->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q107->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q107->get();
   }


     $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

//ad group  

elseif($condition === "contain" && $filter === "adgroup"){

        $q108 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q108->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q108->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q108->get();
   }
$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "adgroup"){

        $q109 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q109->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q109->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q109->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "adgroup"){

        $q110 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q110->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q110->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q110->get();
   }


    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "adgroup"){

        $q112 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q112->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q112->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q112->get();
   }


    $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "adgroup"){

        $q113 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("ad_group","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q113->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q113->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q113->get();
   }

 $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

//keyword

elseif($condition === "contain" && $filter === "keyword"){

        $q114 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q114->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q114->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q114->get();
   }


   $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "doesnotcontain" && $filter === "keyword"){

        $q116 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","not like","%$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q116->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q116->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q116->get();
   }

      $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "is" && $filter === "keyword"){

        $q117 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q117->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q117->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q117->get();
   }

$data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

    elseif($condition === "startwith" && $filter === "keyword"){

        $q119 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","$having%")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q119->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q119->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q119->get();
   }


   $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }
    elseif($condition === "endwith" && $filter === "keyword"){

        $q120 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("keyword","like","%$having")
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $filter_leads = $q120->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $filter_leads = $q120->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $filter_leads = $q120->get();
   }

 $data = array();

foreach($filter_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('exporttoday', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();

    }

    }



}


}
