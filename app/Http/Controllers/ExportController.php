<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Lead;
use Carbon\Carbon;
use App\BrandUser;
use App\Role;
use Auth;


class ExportController extends Controller
{
     public function __construct(Request $request, Lead $lead){
        $this->lead = $lead;
    }
    /*
    export all the details
     */
    public function exportcsvleads(Request $request){
         $q1 = Lead::orderBy("created_at",  "ASC")->where("created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfMonth())
        ->where("created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfMonth());

        if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $all_leads_csv = $q1->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $all_leads_csv = $q1->where("brand_id","=",$request->session()->get("brand_id"))->get()->toArray();
   }else {
        $all_leads_csv = $q1->get()->toArray();
   }

        \Excel::create('Leads', function($excel) use($all_leads_csv){

        $excel->sheet('Excel sheet', function($sheet) use($all_leads_csv){

      $sheet->fromArray($all_leads_csv);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    }
    public function exportallleads(Request $request){
        $q2 = Lead::orderBy("created_at",  "ASC")->where("created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfMonth())
        ->where("created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfMonth()); 

  if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $all_leads = $q2->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $all_leads = $q2->where("brand_id","=",$request->session()->get("brand_id"))->get()->toArray();
   }else {
        $all_leads = $q2->get()->toArray();
   }

        \Excel::create('Leads', function($excel) use($all_leads){

        $excel->sheet('Excel sheet', function($sheet) use($all_leads){

      $sheet->fromArray($all_leads);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');
    }

    public function todaysreportexport(Request $request){

        $q3 =  DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $daily_leads = $q3->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $daily_leads = $q3->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $daily_leads = $q3->get();
   }  

$data = array();

foreach($daily_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('DailyLeads', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    return Redirect::back();
    }

    public function todaysreportcsv(Request $request){

        $q4 =  DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $daily_leads_csv = $q4->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $daily_leads_csv = $q4->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $daily_leads_csv = $q4->get();
   } 

$data = array();

foreach($daily_leads_csv as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('DailyLeads', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    return Redirect::back();
    }


    public function monthlyreportexport(Request $request){

        $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfMonth();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfMonth();
    $q5 =  DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",$c)
->where("leads.created_at","<",$c1)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $monthly_leads = $q5->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $monthly_leads = $q5->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $monthly_leads = $q5->get();
   }


$data = array();

foreach($monthly_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('MonthlyLeads', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    }

    public function monthlyreportexportcsv(Request $request){

        $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfMonth();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfMonth();

    $q6 =  DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",$c)
->where("leads.created_at","<",$c1)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $monthly_leads_csv = $q6->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $monthly_leads_csv = $q6->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $monthly_leads_csv = $q6->get();
   }


$data = array();

foreach($monthly_leads_csv as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('MonthlyLeads', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    }


    public function weeklyreportexport(Request $request){

        $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfWeek();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfWeek();

    $q7 =  DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",$c)
->where("leads.created_at","<",$c1)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $weekly_leads = $q7->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $weekly_leads = $q7->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $weekly_leads = $q7->get();
   }


$data = array();

foreach($weekly_leads as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('Weeklyleads', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');

    }

     public function weeklyreportexportcsv(Request $request){

        $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfWeek();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfWeek();

    $q8 =  DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",$c)
->where("leads.created_at","<",$c1)
->groupBy("leads.created_at")
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $weekly_leads_csv = $q8->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $weekly_leads_csv = $q8->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $weekly_leads_csv = $q8->get();
   }


$data = array();

foreach($weekly_leads_csv as $daily)
{
    $results["id"] = $daily->id;
    $results["date"] = $daily->creation;
    $results["customer_name"] = $daily->customer_name;
    $results["city"] = $daily->city;
    $results["email"] = $daily->email;
    $results["contact"] = $daily->contact;
    $results["source"] = $daily->source;
    $results["campaign_name"] = $daily->campaign;
    $results["adGroup"] = $daily->ad_group;
    $results["ad"] = $daily->ad;
    $results["keyword"] = $daily->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('Weeklyleads', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');

    }

     public function exportleadsperday(Request $request){

        $input1  = $request["daterangepicker_start1"];
//var_dump($input1);
    $input2 = $request["daterangepicker_end1"];

    if(isset($input1) && isset($input2)){
    \Config::set('app.timezone',  'UTC');
    $startDate  = strtotime($input1);
    $endDate = strtotime($input2);

    \Config::set('app.timezone',  'Asia/Kolkata');
    $startDate = date('Y-m-d H:i:s', $startDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    $endDate = new \Datetime($endDate);
    $endDate  = $endDate->modify("+1 DAY");
    
        $q9 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $leads_per_day_count = $q9->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $leads_per_day_count = $q9->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $leads_per_day_count = $q9->get();
   }


$data = array();

foreach($leads_per_day_count as $datewise)
{
    $results["id"] = $datewise->id;
    $results["date"] = $datewise->creation;
    $results["customer_name"] = $datewise->customer_name;
    $results["city"] = $datewise->city;
    $results["email"] = $datewise->email;
    $results["contact"] = $datewise->contact;
    $results["source"] = $datewise->source;
    $results["campaign_name"] = $datewise->campaign;
    $results["adGroup"] = $datewise->ad_group;
    $results["ad"] = $datewise->ad;
    $results["keyword"] = $datewise->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('Datewiseleads', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('xls');
      
    }
    }


    public function exportleadsperdaycsv(Request $request){

        $input1  = $request["daterangepicker_start1"];
//var_dump($input1);
    $input2 = $request["daterangepicker_end1"];

    if(isset($input1) && isset($input2)){
    \Config::set('app.timezone',  'UTC');
    $startDate  = strtotime($input1);
    $endDate = strtotime($input2);

    \Config::set('app.timezone',  'Asia/Kolkata');
    $startDate = date('Y-m-d H:i:s', $startDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    $endDate = new \Datetime($endDate);
    $endDate  = $endDate->modify("+1 DAY");
    
        $q10 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $leads_per_day_count_csv = $q10->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $leads_per_day_count_csv = $q10->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $leads_per_day_count_csv = $q10->get();
   }


$data = array();

foreach($leads_per_day_count_csv as $datewise)
{
    $results["id"] = $datewise->id;
    $results["date"] = $datewise->creation;
    $results["customer_name"] = $datewise->customer_name;
    $results["city"] = $datewise->city;
    $results["email"] = $datewise->email;
    $results["contact"] = $datewise->contact;
    $results["source"] = $datewise->source;
    $results["campaign_name"] = $datewise->campaign;
    $results["adGroup"] = $datewise->ad_group;
    $results["ad"] = $datewise->ad;
    $results["keyword"] = $datewise->keyword;
    $data[] = (array)$results;
}   

    \Excel::create('Datewiseleads', function($excel) use($data){

    $excel->sheet('Excel sheet', function($sheet) use($data){

      $sheet->fromArray($data);
      $sheet->setOrientation('landscape');

    });

    })->export('csv');
      
    }
    }

   
}
