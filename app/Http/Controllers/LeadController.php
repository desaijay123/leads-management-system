<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lead;
use App\Leadsheader;
use Jenssegers\Agent\Agent;
use DB;
use Carbon\Carbon;
use Auth;
use App\Role;
use App\BrandUser;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct(Lead $lead){
        $this->lead = $lead;
        //$this->middleware("admin");

    }
    
    public function index(Request $request)
    {
        if(is_null($request->session()->get("brand_id")) && Auth::user()->hasRole('super_admin')){
        return Redirect::to("/brands");
    }
    elseif (is_null($request->session()->get("brand_id")) && Auth::user()->hasRole('brand_manager')) {
        return Redirect::to("/brands/products");
    }

         $query =DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads") )))
->where("created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfMonth())
->where("created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfMonth())
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $leads_per_day_count = $query->whereIn("campaign_id",$campaign_id);
}
//dd($leads_per_day_count->total_leads);
   }


if(!is_null($request->session()->get("brand_id"))) {
       $leads_per_day_count = $query->where("brand_id","=",$request->session()->get("brand_id"))->get();

   }else {
        $leads_per_day_count = $query->get();
   }
   //dd($request->session()->get("brand_id"));



    //dd($leads_per_day_count);
        $query1 = Lead::orderBy("created_at",  "DESC")->where("created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfMonth())
        ->where("created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfMonth());

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);
   if(count($campaign_id) > 0){
       $leads = $query1->whereIn("campaign_id",$campaign_id);
}

   }


if(!is_null($request->session()->get("brand_id"))) {
       $leads = $query1->where("brand_id","=",$request->session()->get("brand_id"))->take(10)->get();
   }else {
        $leads = $query1->take(10)->get();
   }

        $query2 = $this->lead->where("created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfMonth())
        ->where("created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfMonth());


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $count_leads = $query2->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $count_leads = $query2->where("brand_id","=",$request->session()->get("brand_id"))->count();
   }else {
        $count_leads = $query2->count();
   }

if(empty($leads)){
    $photoAreas = array('/images/image1.jpg', '/images/image2.jpg', '/images/image3.jpg');
    $randomNumber = array_rand($photoAreas);
    $randomImage = $photoAreas[$randomNumber];
    return view("dashboard.dailyempty", compact("randomImage"));

}
        return view("dashboard.lead",compact("leads","count_leads","leads_per_day_count"));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $agent = new Agent();
        $lead = new Lead;
        $lead->campaign_id = $request["campaign_id"];
        $lead->customer_name = $request["customer_name"];
        $lead->brand_id = $request["brand_id"];
        $lead->city = $request["city"];
        $lead->source = $request["source"];
        $lead->email = $request["email"];
        $lead->campaign_name = $request["campaign_name"];
        $lead->ad_group = $request["ad_group"];
        $lead->keyword = $request["keyword"];
        $lead->car_brand = $request["car_brand"];
        $lead->member = $request["member"];
        $lead->contact = $request["contact"];
        $lead->save();
        $header = new Leadsheader;
        $header->lead_id = $lead->id;
        $header->user_agent = $request->header("User-Agent");
        $header->host = $request->ip();
        $header->referer = $request->server('HTTP_REFERER');
        $header->operating_system = $agent->platform();
        $header->device_type = $agent->device();
        $header->browser = $agent->browser();
        $header->save();
        return Redirect::to("http://mymotorinsurancequote.com/libertyvideocongeneralinsurance/thankyoupage.html");
    }

    /**
     * Display leads paer day.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function leadsperday(Request $request){

        $input1  = $request["daterangepicker_start"];
//var_dump($input1);
    $input2 = $request["daterangepicker_end"];

    if(isset($input1) && isset($input2)){
    \Config::set('app.timezone',  'UTC');
    $startDate  = strtotime($input1);
    $endDate = strtotime($input2);

    \Config::set('app.timezone',  'Asia/Kolkata');
    $startDate = date('Y-m-d H:i:s', $startDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    $endDate = new \Datetime($endDate);
    $endDate  = $endDate->modify("+1 DAY");
    
        $query3 = DB::table("leads")->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("leads.created_at")
->orderBy("leads.created_at","desc");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $leads_per_day_count = $query3->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $leads_per_day_count = $query3->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $leads_per_day_count = $query3->get();
   }

//graph
$query4 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate)
->groupBy("creation")
->orderBy("creation");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $leads_per_day = $query4->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $leads_per_day = $query4->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $leads_per_day = $query4->get();
   }



$query5 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate);


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $total_leads = $query5->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $total_leads = $query5->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $total_leads = $query5->get();
   }

if(empty($leads_per_day)){
    $photoAreas = array('/images/image1.jpg', '/images/image2.jpg', '/images/image3.jpg');
    $randomNumber = array_rand($photoAreas);
    $randomImage = $photoAreas[$randomNumber];
    return view("dashboard.dailyempty", compact("randomImage"));

}

        return view("dashboard.date1", compact("leads_per_day_count", "leads_per_day","total_leads"));  
    }
    }

     /**
     * Display total leads via date .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function leadsviadate(Request $request){
            $input1  = $request["daterangepicker_start"];
//var_dump($input1);
    $input2 = $request["daterangepicker_end"];

    if(isset($input1) && isset($input2)){
    \Config::set('app.timezone',  'UTC');
    $startDate  = strtotime($input1);
    $endDate = strtotime($input2);

    \Config::set('app.timezone',  'Asia/Kolkata');
    $startDate = date('Y-m-d H:i:s', $startDate);
    $endDate = date('Y-m-d H:i:s', $endDate);
    $endDate = new \Datetime($endDate);
    $endDate  = $endDate->modify("+1 DAY");

$query6 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("leads.created_at",">=",$startDate)
->where("leads.created_at","<",$endDate);


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $total_leads = $query6->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $total_leads = $query6->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $total_leads = $query6->get();
   }



return view("dashboard.date2", compact("total_leads")); 
}
     }
/*
  Todays leads
 */
public function todaysleads(Request $request){

$query7 = DB::table("leads")
                ->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $daily_leads_graph = $query7->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $daily_leads_graph = $query7->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $daily_leads_graph = $query7->get();
   }

    $query8 =DB::table("leads")
->select((array(DB::Raw('DATE_FORMAT(leads.created_at,"%m/%d/%Y") as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"))
->groupBy("leads.created_at")
->orderBy("leads.created_at");


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $daily_name_table = $query8->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $daily_name_table = $query8->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $daily_name_table = $query8->get();
   }

if(empty($daily_name_table) ){
    $photoAreas = array('/images/image1.jpg', '/images/image2.jpg', '/images/image3.jpg');
    $randomNumber = array_rand($photoAreas);
    $randomImage = $photoAreas[$randomNumber];
    return view("dashboard.dailyempty", compact("randomImage"));

}


return view("dashboard.daily", compact("daily_leads_graph","daily_name_table"));




}
/*
total today leads
 */

    public function todaystotalleads(Request $request){

    $query9 =DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("leads.created_at",">=",Carbon::today("Asia/Kolkata"))
->where("leads.created_at","<",Carbon::tomorrow("Asia/Kolkata"));

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $daily_total_leads = $query9->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $daily_total_leads = $query9->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $daily_total_leads = $query9->get();
   }



return view("dashboard.daily1", compact("daily_total_leads"));

}

    public function monthlyleads(Request $request){
        $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfMonth();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfMonth();
    
    $query10 =DB::table("leads")->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfMonth())
->where("leads.created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfMonth())
->groupBy("creation")
->orderBy("creation");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $monthly_leads_graph = $query10->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $monthly_leads_graph = $query10->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $monthly_leads_graph = $query10->get();
   }



    $query11 = DB::table("leads")
->select((array(DB::Raw('DATE_FORMAT(leads.created_at,"%m/%d/%Y") as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfMonth())
->where("leads.created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfMonth())
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $monthly_name_table = $query11->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $monthly_name_table = $query11->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $monthly_name_table = $query11->get();
   }

    if(empty($monthly_name_table) ){
    $photoAreas = array('/images/image1.jpg', '/images/image2.jpg', '/images/image3.jpg');
    $randomNumber = array_rand($photoAreas);
    $randomImage = $photoAreas[$randomNumber];
    return view("dashboard.dailyempty", compact("randomImage"));

}
   
return view("dashboard.monthly", compact("monthly_leads_graph","monthly_name_table"));

    }


public function monthlytotalleads(Request $request){
    $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfMonth();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfMonth();
    
    $query12 =DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("leads.created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfMonth())
->where("leads.created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfMonth());

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $monthly_total_leads = $query12->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $monthly_total_leads = $query12->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $monthly_total_leads = $query12->get();
   }

return view("dashboard.monthly1", compact("monthly_total_leads"));
}

public function weeklyleads(Request $request){
    $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfWeek();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfWeek();
    
    $query13 = DB::table("leads")
                ->select((array(DB::Raw('DATE_FORMAT(leads.created_at,"%m/%d/%Y") as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfWeek())
->where("leads.created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfWeek())
->groupBy("creation")
->orderBy("creation");
// dd($query13->get());

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $weekly_leads_graph = $query13->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $weekly_leads_graph = $query13->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $weekly_leads_graph = $query13->get();
   }
   
    $query14 =DB::table("leads")
->select((array(DB::Raw('DATE_FORMAT(leads.created_at,"%m/%d/%Y") as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"), DB::Raw("leads.id as id"),DB::Raw("leads.customer_name as customer_name" ),DB::Raw("leads.city as city"), DB::Raw("leads.email as email"),DB::Raw("leads.contact as contact"),DB::Raw("leads.source as source"),DB::Raw("leads.campaign_name as campaign"),DB::Raw("leads.ad_group as ad_group"),DB::Raw("leads.ad as ad"),DB::Raw("leads.keyword as keyword") )))
->where("leads.created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfWeek())
->where("leads.created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfWeek())
->groupBy("leads.created_at")
->orderBy("leads.created_at");

if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $weekly_name_table = $query14->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $weekly_name_table = $query14->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $weekly_name_table = $query14->get();
   }

    if(empty($weekly_name_table) ){
    $photoAreas = array('/images/image1.jpg', '/images/image2.jpg', '/images/image3.jpg');
    $randomNumber = array_rand($photoAreas);
    $randomImage = $photoAreas[$randomNumber];
    return view("dashboard.dailyempty", compact("randomImage"));

}
   
  
return view("dashboard.weekly", compact("weekly_leads_graph","weekly_name_table"));

}


public function weeklytotalleads(Request $request){
    $start = Carbon::now("Asia/Kolkata");
    $c = Carbon::parse($start,"Asia/Kolkata")->startOfWeek();
    $c1 = Carbon::parse($start,"Asia/Kolkata")->endOfWeek();
    
    $query15 = DB::table("leads")
->select((array(DB::Raw('DATE(leads.created_at) as creation'), 
DB::Raw("COUNT(leads.id) as total_leads"))))
->where("leads.created_at",">=",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->startOfWeek())
->where("leads.created_at","<",Carbon::parse(Carbon::now("Asia/Kolkata"),"Asia/Kolkata")->endOfWeek());


if(!is_null($request->session()->get("campaign_id"))) {
    $campaign_id = explode(",", $request->session()->get("campaign_id"));
    //$campaign_id_1 = array_values($campaign_id);

   if(count($campaign_id) > 0){
       $weekly_total_leads = $query15->whereIn("campaign_id",$campaign_id);
}
   }


if(!is_null($request->session()->get("brand_id"))) {
       $weekly_total_leads = $query15->where("brand_id","=",$request->session()->get("brand_id"))->get();
   }else {
        $weekly_total_leads = $query15->get();
   }

return view("dashboard.weekly1", compact("weekly_total_leads"));

}




















































}
