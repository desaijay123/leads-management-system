<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\BrandUser;
use App\RoleUser;
use App\Brand;
use Response;
use Illuminate\Support\Facades\Redirect;
use App\Campaign;

class UserController extends Controller
{
   public function __construct(User $user){
    $this->user = $user;
   }

   public function getUserpage(Request $request){
    
    $user = User::all();
    $role = Role::all();
    $brand = Brand::all();
    $campaign = Campaign::all();
    $brand_id = $request["brand_id"]; //this comes as a id
   
    $campaign_object = Campaign::where("brand_id", '=', $brand_id)->get();

    return view("admin.user", compact("user", "role","brand","campaign","campaign_object"));

    }

    public function getbrandscampaign(Request $request){
       $brand_id = $request["brand_id"]; //this comes as a id
      // dd($brand_id);

    $campaign_object = Campaign::where("brand_id", '=', $brand_id)->get();
    
    return Response::json($campaign_object);
  }
    // public function getbrandcampaigns(Request $request){
      

      
    //   return view("admin.user", compact("campaign_object"));
    // }


    public function postRegister(Request $request){

       $campaign_id = $request["campaign_id"]; 
       // dd($campaign_id);
       // die();
        $rules = array( 'name' => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'brand_id' => 'required|integer',
            'role_id'  => 'required|integer');
    
     $this->validate($request, $rules);

     
        $user = new User;

        $user->name = $request["name"];
        $user->email = $request["email"];
        $user->password = bcrypt("passw0rd");
        
       // $branduser = new branduser
       if($user->save()){
        $brand_id = $request["brand_id"];
        $campaign_id = $request["campaign_id"]; //array

      $brand = new BrandUser;
      $brand->brand_id = $brand_id;
      $brand->user_id = $user->id; 
      if(empty($campaign_id)){
      $brand->campaign_id = "";
      }
      else{  
      $brand->campaign_id = implode(",",$campaign_id);
    }
      $brand->save();
      
//        $user->branduser()->create(["brand_id"=>$brand_id]);
      $role_id = $request["role_id"];
      $role = new RoleUser;
      $role->role_id = $role_id;
      $role->user_id = $user->id;
      $role->save();

      $request->session()->put('user_id', $user->id);
      $request->session()->put('campaign_id', $campaign_id);
      $request->session()->put('brand_id', $brand_id);
 }
  return Response::json(array('success' => true), 200);

}

public function revokeuserrole(Request $request, $id){
    $role = $request["userroles"]; //get roles in array
//dd($role);
    $user_object = User::find($id); //find the id
    //find the id
  
        $role_object = Role::where("id", $role)->firstOrFail();

        $user_object->revokeRole($role_object);


    return Redirect::back();
}




       
   

}
