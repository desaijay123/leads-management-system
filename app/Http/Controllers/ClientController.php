<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\BrandUser;
class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setclient(Request $request, $id)
    {
        $id = intval($id);

        
        if( $id > 0){
          
     $request->session()->put("brand_id", $id);
      //dd($request->session()->get("brand_id"));
}
else{
    $request->session()->forget("brand_id");
}
        return back();
    }

    public function setclientlogin(Request $request)
    {
       $id = intval($request->input("brand"));
       //dd($id);
       //
        if( $id > 0){
          
     $request->session()->put("brand_id", $id);
     //dd($request->session()->get("brand_id"));
}
else{
    $request->session()->forget("brand_id");
}
        return Redirect::to("/dashboard");
    }

    public function setbrandmanagerlogin(Request $request){
$branduser = BrandUser::where("user_id","=",Auth::id())->first();
    

         $id = intval($request->input("product"));
       //dd($id);
        if( $id > 0){
    $request->session()->put("campaign_id", $id);
     $request->session()->put("brand_id",$branduser->brand_id);
     //dd($request->session()->get("brand_id"));
}
else{
     $request->session()->forget("campaign_id");
      $request->session()->forget("brand_id");
}
        return Redirect::to("/dashboard");
    }

     public function setclientmanager(Request $request, $id)
    {
        $branduser = BrandUser::where("user_id","=",Auth::id())->first();
        $id = intval($id);

        if( $id > 0){
          
    $request->session()->put("campaign_id", $id);
     $request->session()->put("brand_id",$branduser->brand_id);
      //dd($request->session()->get("brand_id"));
}
else{
      $request->session()->forget("campaign_id");
      $request->session()->forget("brand_id");
}
        return back();
       
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
