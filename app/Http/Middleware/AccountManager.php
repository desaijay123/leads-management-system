<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Redirect;

class AccountManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
         if(Auth::check()){
         $user  = $request->user();
        if($user->isAccountmanager()){
            return $next($request);
        }
    }
        return Redirect::to("/dashboard");
    }
}
