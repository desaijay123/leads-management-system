<?php 
namespace App\Http\ViewComposers;
use Auth;
use Illuminate\Contracts\View\View;
use App\Brand;
use Illuminate\Http\Request;
use App\BrandUser;
use App\Campaign;
class UsernameComposer{

	public function compose(View $view){
		$view->with("name", Auth::user()->name);
}

	public function brands(View $view){
		$view->with("brands", Brand::all());
}

	public function products(View $view){
		if(Auth::user()->hasRole("brand_manager") || Auth::user()->hasRole("product_manager")){
		$query = BrandUser::where("user_id","=",Auth::user()->id);
	
	if(session()->has("brand")){
		$product = $query->where("brand_id","=",$request->session()->get("brand_id"))->first();
	}
	else{
		$product = $query->first();
	}
		$campaign = explode(",",$product->campaign_id);
	
	//dd($campaign);
		$q = Campaign::whereIn("id",$campaign)->get();
		$view->with("q", $q);
	}
}

}