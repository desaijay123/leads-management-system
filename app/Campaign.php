<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = "campaigns";

    public function brands(){
    	return $this->belongsTo("App\Brand","campaign_id");
    }

     public function branduser(){
    	return $this->hasMany("App\Branduser");
    }


}
