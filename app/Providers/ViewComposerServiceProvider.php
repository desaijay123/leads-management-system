<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeUsername();
    }
    /*
    composing the user name on the header using 
    view and dedicated viewcomposer serviceprovider 
     */

    public function composeUsername(){
        view()->composer("layouts.dashboard",'App\Http\ViewComposers\UsernameComposer@compose');
        view()->composer("layouts.usersettings",'App\Http\ViewComposers\UsernameComposer@compose');
        view()->composer("admin.userprofile",'App\Http\ViewComposers\UsernameComposer@compose');
        view()->composer("layouts.dashboard",'App\Http\ViewComposers\UsernameComposer@brands');
        view()->composer("layouts.usersettings",'App\Http\ViewComposers\UsernameComposer@brands');
        view()->composer("admin.brands",'App\Http\ViewComposers\UsernameComposer@brands');
        view()->composer("layouts.dashboard",'App\Http\ViewComposers\UsernameComposer@products');
        view()->composer("layouts.usersettings",'App\Http\ViewComposers\UsernameComposer@products');
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
