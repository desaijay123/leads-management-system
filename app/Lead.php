<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Lead extends Model
{
	protected $table = "leads";
	protected $dates = ['created_at', 'updated_at'];
	
       protected $fillable = ['customer_name', 'email', 'contact','source','ad_group','keyword','ad','car_brand','member','city','campaign_id','push_status','age','gender','campaign_name'];

       public function addLead($lead){
       	$this->save();
       }
       

       public function setCreatedAtAttribute($date){
       		$this->attributes["created_at"] = Carbon::parse($date);
       }

    public function leadsheader(){
    	return $this->hasOne(Leadsheader::class);
    }

}
