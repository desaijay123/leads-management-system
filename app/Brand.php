<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
	protected $table = "brands";
	protected $fillable = ["name", "label"];

    public function users(){
    	return $this->belongsToMany(User::class);
    }

     public function campaigns(){
    	return $this->hasMany("App\Campaign","brand_id");
    }

    public function branduser(){
    	return $this->belongsToMany(Brand::class);
    }
}
