<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Cache-control" content="public">
	<title>LMS</title>

	<link rel="stylesheet" href="/css/reset.css">
	<link rel="stylesheet" href="/css/font-awesome.min.css">	
	<link rel="stylesheet" href="/css/fonts.css">
	<link rel="stylesheet" href="/css/bootstrap.css">
		<link rel="stylesheet" href="/css/custom.css">

	<link href="/css/bootstrap-material-design.css" rel="stylesheet">
	<link href="/css/ripples.css" rel="stylesheet">
	<link rel="stylesheet" href="/css/all.css">
	<script src="/js/modernizr.custom.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/ie7.css">
	<link rel="stylesheet" type="text/css" href="/css/iconfont.css">
	<link rel="stylesheet" type="text/css" href="/css/responsive.css">
	@yield("header")
</head>
<body>
	<div class="container">
		
		@yield("content")
		
	</div>

	
	<script src="/js/jquery-2.1.3.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/material.js"></script>	
	<script src="/js/ripples.js"></script>	
	<script src="/js/hideShowPassword.min.js"></script> 
	<script src="/js/custom.js"></script>
	<script src="/js/ie7.js" ></script>
	<script type="text/javascript">
	  $.material.init();
	</script>
	
		@yield("footer")
</body>
</html>