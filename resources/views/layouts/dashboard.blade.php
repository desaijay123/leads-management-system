<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Cache-control" content="public">
		 <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<title>LMS</title>
		<link rel="stylesheet" href='{{URL::asset( "css/reset.css") }}'>
		<link rel="stylesheet" href='{{URL::asset("css/font-awesome.min.css") }}'>	
		<link rel="stylesheet" href='{{URL::asset("css/fonts.css") }}'>
		<link rel="stylesheet" href='{{URL::asset("css/bootstrap.css") }}'>
		<link href='{{URL::asset("css/bootstrap-material-design.css")}}'  rel="stylesheet">
		<link href='{{URL::asset("css/ripples.css") }}'rel="stylesheet">
    <link href="{{URL::asset('css/jquery.dropdown.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="all" href='{{URL::asset("css/daterangepicker.css") }}' />
    <!-- Morris chart -->
    <link href='{{ URL::asset("css/morris.css")}}' rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href='{{ URL::asset("css/jquery.mCustomScrollbar.css") }}'>
    <!-- Morris chart -->

      <!-- DataTables -->
     <link rel="stylesheet" href='{{ URL::asset("css/dataTables.bootstrap.css") }}'>
    <link rel="stylesheet" href='{{ URL::asset("/css/dashboard.css") }}'>

    <link rel="stylesheet" type="text/css" href='{{ URL::asset("css/custom.css") }}'>
		<script src='{{URL::asset("js/modernizr.custom.js") }}'></script>
    <link rel="stylesheet" href='{{ URL::asset("css/multiple-select.css") }}' />
  <link rel="stylesheet" type="text/css" href="/css/responsive1.css">
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="/css/ie7.css">
     <script src="/js/ie7.js" ></script>
<![endif]-->

  <link rel="stylesheet" type="text/css" href='{{ URL::asset("css/iconfont.css") }}'>
		@yield("head")
	</head>

	<body>
    <div class="wrapper">
      <header class="main_header">
          <div class="header clearfix">
            <div class="header_logo floatL">
              <a href="{{url('/dashboard')}}">
                <img src="/images/pivotroots_dashboard.png" alt="pivotroots">
              </a>
            </div><!--header_logo Ends-->
            <div class="navbar navbar-default floatL menu_left">
             <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <!--a class="navbar-brand" href="javascript:void(0)">Home</a-->
                </div>
                <div id="top"class="navbar-collapse collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">
                      <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                        <a href="{{url('/dashboard')}}" id="home">Home</a>
                      </li>
                      <li class="{{ Request::is('leads') ? 'active' : '' }}">
                        <a href="{{url('/leads') }}" id="leads">Leads</a>
                      </li>
                      @if(Auth::user()->hasRole("super_admin"))
                      <li class="dropdown menu3">
                            <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Client
                              <b class="caret"></b>
                             </a>
                            <ul class="dropdown-menu">
                            @foreach($brands as $b)
                            <li>
                                <a href="/setclient/{{$b->id}}">{{ucfirst($b->name)}}</a>
                              </li>
                              @endforeach
                            </ul>

                          </li>
                          @elseif(Auth::user()->hasRole("brand_manager"))
                            <li class="dropdown menu3">
                            <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Products
                              <b class="caret"></b>
                             </a>
                            <ul class="dropdown-menu">
                           @foreach($q as $b)
                            <li>
                                <a href="/setclient/{{$b->id}}">{{ucfirst($b->campaign_name)}}</a>
                              </li>
                             @endforeach
                              
                            </ul>
                          </li>
                          @endif
                    </ul>
                  </div>
              </div>
            </div>
                  <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu floatR">     
            <ul class="nav navbar-nav">
              <li class="dropdown search-menu">
                <form id="search" action="#" method="post">
                    <div id="label"><label for="search-terms" id="search-label">search</label></div>
                    <div id="input"><input type="text" name="search-terms" id="search-terms" placeholder="Enter search terms..."></div>
                </form>
              </li>
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell"></i>
                  <!--span class="label label-warning">10</span-->
                </a>
              </li>
              <!-- Control Sidebar Toggle Button -->
               <!-- User Account: style can be found in dropdown.less -->
             <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!--i class="fa fa-user"></i-->
                  <span>{{$name}} <i class="caret"></i></span>
              </a>
              <ul class="dropdown-menu dropdown-custom dropdown-menu-right"> 
                  <li>                
                    <a data-toggle="modal" href="/user/profile">
                      <i class="fa fa-cog pull-right"></i>
                        Settings
                    </a>
                  </li>

                 <li class="divider"></li>

                  <li>
                    <a href="/auth/logout">
                      <i class="fa fa-sign-out fa-fw pull-right"></i> Logout
                    </a>
                  </li>
                </ul>
              </li>
           </ul>   
        </div>
  </div>

       @yield("header")
   


@yield("content")
@yield("filter")

@include("layouts.footertext")

	</div><!--wrapper Ends-->

		<script src="{{URL::asset('js/jquery-2.1.3.min.js')}}"></script>
		<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.slimscroll.min.js')}}"></script>
		<script src="{{URL::asset('js/material.js')}}"></script>	
		<script src="{{URL::asset('js/ripples.js')}}"></script>
    <script src="{{URL::asset('js/jquery.dropdown.js')}}"></script>
    <script src="{{URL::asset('js/classie.js')}}"></script>
    <script src= "{{URL::asset('js/search.js')}}"></script>
    <script  src="{{URL::asset('js/moment.js')}}"></script>
    <script src="{{URL::asset('js/daterangepicker.js')}}"></script>
    <script src= "{{URL::asset('js/chart.js')}}"></script>
    <script src="{{URL::asset('js/dashboard.js')}}"></script>
    <script src="{{URL::asset('js/overview.js') }}"></script>
    <script src="{{ URL::asset('js/spin.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
  <script src="{{ URL::asset('js/multiple-select.js') }}"></script>

  <script type="text/javascript">
      $.material.init();  
        $(".filter-select, .filter-condition").dropdown({"optionClass": "withripple"});

    </script>

	@yield("footer")
    </body>

</html>