<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		 <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<title>LMS</title>
		<link rel="stylesheet" href="/css/reset.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">	
		<link rel="stylesheet" href="/css/fonts.css">
		<link rel="stylesheet" href="/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="/css/custom.css">
		<link href="/css/bootstrap-material-design.css" rel="stylesheet">
    <link href="/css/mdb.css" rel="stylesheet">
		<link href="/css/ripples.css" rel="stylesheet">
    <link href="/css/jquery.dropdown.css" rel="stylesheet">   
 <link rel="stylesheet" href="/css/dashboard.css">	
		<link href="/css/prism.css" rel="stylesheet">
<link rel="stylesheet" href="/css/multiple-select.css" />
		

	</head>

	<body>
 <div class="wrapper">
      <div class="main_header">
        <div class="header clearfix">
                <div class="header_logo floatL">
                  <a href="/dashboard">
                    <img src="/images/pivotroots_dashboard.png" alt="pivotroots">
                  </a>
                </div><!--header_logo Ends-->
                <div class="navbar navbar-default floatL menu_left">
                 <div class="container-fluid">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <!--a class="navbar-brand" href="javascript:void(0)">Home</a-->
                    </div>
                    <div class="navbar-collapse collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav">
                          <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                        <a href="{{url('/dashboard')}}" id="home">Home</a>
                      </li>
                      <li class="{{ Request::is('leads') ? 'active' : '' }}">
                        <a href="{{url('/leads') }}" id="leads">Leads</a>
                      </li>
                          @if(Auth::user()->hasRole("super_admin"))
                      <li class="dropdown menu3">
                            <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Client
                              <b class="caret"></b>
                             </a>
                            <ul class="dropdown-menu">
                            @foreach($brands as $brand)
                            <li>
                                <a href="/setclient/{{$brand->id}}">{{$brand->name}}</a>
                              </li>
                             @endforeach
                            </ul>
                          </li>
                          @elseif(Auth::user()->hasRole("brand_manager"))
                            <li class="dropdown menu3">
                            <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Products
                              <b class="caret"></b>
                             </a>
                            <ul class="dropdown-menu">
 							@foreach($q as $b)
                            <li>
                                <a href="/setclient/{{$b->id}}">{{ucfirst($b->campaign_name)}}</a>
                              </li>
                             @endforeach
                            </ul>
                          </li>
                          @endif
                        </ul>
                      </div>
                  </div>
                </div>
                      <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu floatR">     
                <ul class="nav navbar-nav">
                  <li class="dropdown search-menu">
                    <form id="search" action="#" method="post">
                        <div id="label"><label for="search-terms" id="search-label">search</label></div>
                        <div id="input"><input type="text" name="search-terms" id="search-terms" placeholder="Enter search terms..."></div>
                    </form>
                  </li>
                  <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-bell"></i>
                      <!--span class="label label-warning">10</span-->
                    </a>
                  </li>
                  <!-- Control Sidebar Toggle Button -->
                   <!-- User Account: style can be found in dropdown.less -->
                 <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--i class="fa fa-user"></i-->
                        <span>{{$name}}<i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu dropdown-custom dropdown-menu-right"> 
                        <li>                
                          <a data-toggle="modal" href="/user/profile">
                            <i class="fa fa-cog fa-fw pull-right"></i>
                              Settings
                          </a>
                        </li>

                       <li class="divider"></li>

                      <li>
                        <a href="/auth/logout">
                          <i class="fa fa-sign-out fa-fw pull-right"></i> Logout
                        </a>
                      </li>
                    </ul>
                  </li>
               </ul>           
             </div>
           </div> <!--header Ends-->
      </div>
       <!-- Sidebar navigation -->
    <ul id="slide-out" class="side-nav fixed default-side-nav light-side-nav">        
        <ul class="collapsible collapsible-accordion">
            <li class="waves-effect {{ Request::is('user/profile') ? 'active_side' : '' }}">
              <a class="collapsible-header" href="/user/profile">Profile</a>               
            </li>
         
                @if(Auth::user()->hasRole("super_admin") || Auth::user()->hasRole("account_manager"))
            <li class="waves-effect {{ Request::is('admin/user/info') ? 'active_side' : '' }}">
              <a class="collapsible-header" href="/admin/user/info">User</a>               
            </li> 
            @endif
           
           @if(Auth::user()->hasRole("super_admin"))
            <li class="{{ Request::is('admin/roles/permissions') ? 'active_side' : '' }}">
              <a class="collapsible-header" href="/admin/roles/permissions">Roles & Permission
              </a>
            </li>
           @endif
        </ul>
      
    </ul>
    <!--/. Sidebar navigation -->
     <div class="clearfix"></div>
   @yield("content")
     <div class="clearfix"></div>
   
<div id="footer">     
              <p class="floatR">
                © Copyrights PivotRoots | All Rights Reserved
              </p>
          </div><!--Footer Ends-->
    
    </div>

		<script src="/js/jquery-2.1.3.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>   
    <script src="/js/jquery.slimscroll.min.js"></script>
		<script src="/js/material.js"></script>	
		<script src="/js/ripples.js"></script>
    <script src="/js/mdb.js"></script>
    <script src="/js/jquery.dropdown.js"></script>
    <script src="/js/classie.js"></script>
    <script src="/js/search.js"></script>
    <script type="text/javascript" src="/js/moment.js"></script>
 	<script src="/js/modernizr.custom.js"></script>
    <script src="/js/usersetting.js"></script>
     <script src="/js/prism.js"></script>
	<script src="/js/multiple-select.js"></script>

		<script type="text/javascript">       
        Waves.attach('.sidebar_menu a', ['waves-light']);
        Waves.init();
    </script>
 
      <script type="text/javascript">   
                        
                 //On Scroll Header Fixed
               
                 $(window).scroll(function(){
                    var sticky = $('.main_header'),
                        scroll = $(window).scrollTop();

                    if (scroll >= 72){
                        sticky.addClass('header_fixed');
                      } 
                    else {
                      sticky.removeClass('header_fixed');
                    }
                  });              
             
       </script>
       @yield("footer")
	</body>

</html>
