@extends("layouts.dashboard")
@section("header")
<!--middle_header Starts-->

        <div class="middle_header">
            <div class="middle_left floatL">
              <div class="totalimage_lead floatL"> 
                <img src="images/totalleads.png" alt="totallead">
              </div>
              <div class="totallead_text floatL">
      <div id="total_leads"> <div class="totaltext">total leads - <span> {{$count_leads}} </span></div></div>
              </div>
            </div><!--middle_left Ends-->

            <!--Latest Download Code-->
            <div class="esa1">
              <div class="middle_last floatR">
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" id="exportbycsv" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Download
                  </button>
                  <div class="dropdown-menu" aria-labelledby="download_btn">
                    <li id="export_csv">
                      <a class="dropdown-item">CSV</a>  
                    </li>
                    <li id="export">
                      <a class="dropdown-item">XLS</a>            
                    </li>
                      
                  </div>
                </div>
              </div><!--middle_last Ends-->
            </div>
          <div class="esa">
              <div class="middle_last floatR">
                  <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="expo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Download
                    </button>
                    <div class="dropdown-menu" aria-labelledby="download_btn">
                     <form action="/export/csv/datewise" method="GET" class="ajaxformcsv">
                  <input type="hidden" name="daterangepicker_start1" id="daterangepicker_start1"></input>
                  <input type="hidden" name="daterangepicker_end1" id="daterangepicker_end1"></input>
                  <button id="datewise" class="middle_download_button">
                      <li><a class="dropdown-item">CSV</a></li>
                       </button>
                      </form>
                  <form action="/export/datewise" method="GET" class="ajaxform">
                  <input type="hidden" name="daterangepicker_start1" id="daterangepicker_start1"></input>
                  <input type="hidden" name="daterangepicker_end1" id="daterangepicker_end1"></input>
                  <button id="datewise" class="middle_download_button">
                      <li><a class="dropdown-item">XLS</a></li>
                        </button>
                      </form>
                    </div>
                  </div>
                </div><!--middle_last Ends-->
            </div>
            
            <div class="dailyreport">
             <div class="middle_last floatR">
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" id="download_btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Download
                  </button>
                  <div class="dropdown-menu" aria-labelledby="download_btn">
                    <li id="export_daily_csv">
                      <a class="dropdown-item" href="#">CSV</a>  
                    </li>
                    <li id="export_daily">
                      <a class="dropdown-item" href="#">XLS</a>            
                    </li>   
                  </div>
                </div>
              </div><!--middle_last Ends-->
            </div>
               <div class="monthlyreport">
                <div class="middle_last floatR">
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" id="download_btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Download
                  </button>
                  <div class="dropdown-menu" aria-labelledby="download_btn">
                    <li id="export_monthly_csv">
                      <a class="dropdown-item" href="#">CSV</a>  
                    </li>
                    <li id="export_monthly">
                      <a class="dropdown-item" href="#">XLS</a>            
                    </li>
                      
                  </div>
                </div>
              </div><!--middle_last Ends-->
            </div>

           <!--Latest Download Code-->
           <div class="weeklyreport">
            <div class="middle_last floatR">
              <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" id="download_btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Download
                </button>
                <div class="dropdown-menu" aria-labelledby="download_btn">
                  <li id="export_weekly_csv">
                    <a class="dropdown-item">CSV</a>
                  </li>
                  <li>
                    <a id="export_weekly" class="dropdown-item" href="#">XLS</a>            
                  </li>
                    
                </div>
              </div>
            </div><!--middle_last Ends-->
          </div>

           <!--Latest Download Code-->
          <div class="filterreports">
            <div class="middle_last floatR">
              <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" id="download_btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Download
                </button>
                <div class="dropdown-menu" aria-labelledby="download_btn">
                    <form action="/export/csv/filter/datewise" method="GET" class="ajaxform1csv">
                  <input type="hidden" name="daterangepicker_start2" id="daterangepicker_start2" value=""></input>
                 <input type="hidden" name="daterangepicker_end2" id="daterangepicker_end2"></input><input type="hidden" id="having_val" name="having_val"></input>
                 <input type="hidden" id="filter_val" name="filter_val"></input>
                  <input type="hidden" id="condition_val" name="condition_val"></input>
                   <button id="datewise" class="middle_download_button">
                      <li>
                        <a class="dropdown-item">CSV</a>  
                      </li>
                        </button>
                       </form>

                      <form action="/export/filter/datewise" method="GET" class="ajaxform1">
                    <input type="hidden" name="daterangepicker_start2" id="daterangepicker_start2" value=""></input>
                 <input type="hidden" name="daterangepicker_end2" id="daterangepicker_end2"></input><input type="hidden" id="having_val_csv" name="having_val"></input>
                 <input type="hidden" id="filter_val_csv" name="filter_val"></input>
                  <input type="hidden" id="condition_val_csv" name="condition_val"></input>
                   <button id="datewise" class="middle_download_button">
                        <li><a class="dropdown-item">XLS</a></li>
                        </button>
                         </form>
                  </div>
                </div>
              </div><!--middle_last Ends-->
          </div>
          <div class="middle_right floatR calendar_unit">
             <div id="config-calendar" class="pull-right" style="background: #fff; cursor: pointer; padding: 8px 0 8px 39px; border-bottom: 1px solid #ccc; width: 100%">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                <span></span> <b class="caret"></b>
            </div>
            </div><!--middle_right Ends-->
        </div>

        </header><!--header Ends-->
@endsection

@section("content")
<div id="ack">
  <!--Graph Element Starts-->
    <div class="lead_chart_wrapper">
      <div class="panel">
        <div class="panel-body">           
            <canvas id="linechart" width="75%" height="200"></canvas>
        </div>
      </div>
    </div><!--Graph Element Ends-->
 <div id="dvloader" style="    display: none;
    text-align: center;
    margin-top: 0px;
    background-color: #fff; "><img src="/images/status.gif" > </div>
   <!--adding_filter Starts-->
              <div class="adding_filter">
                 <div class="filter_tabs floatR">
                    <label for="filter_parameters" class="control-label">Select Other Parameters</label>             
                    <select id="filter_parameters" multiple="multiple" class="multiselect">          
                      <option value="1" class="campaign">Campaigns</option>
                      <option value="2" class="adgroup">Ad Groups</option>
                      <option value="3" class="ad">Ad</option>
                      <option value="4" class="keyword">Keywords</option>            
                    </select>
                 
                 </div> <!--filter_tabs Ends-->

       <div class="filter_table">
         <!-- /.box-header -->
            <div class="box-body">
              <table id="table_data" class="table table-bordered table-hover table-striped">
                <thead class="table-inverse">
                  <tr>
                    <th>Leads</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Source</th>
                    <th>Status</th>
                    <th id="show_td_1" class="hide_value">Campaign</th>
                    <th id="show_td_2" class="hide_value">Ad Groups</th>
                    <th id="show_td_3" class="hide_value">Ad</th>
                    <th id="show_td_4" class="hide_value">Keywords</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($leads as $lead)
                  <tr>
                    <td>{{$lead->id}}</td>
                    <td>{{$lead->created_at->format("d/m/Y")}}</td>
                    <td>{{ucfirst($lead->customer_name)}}</td>
                    <td> {{ucfirst(strtolower($lead->city))}}</td>
                    <td>{{$lead->email}}</td>
                    <td>{{$lead->contact}}</td>
                    <td>{{$lead->source}}</td>
                    <td>01</td>
                    <td class="hide show_campaign">{{$lead->campaign_name}}</td>
                    <td class="hide show_adgroup">{{$lead->ad_group}}</td>
                    <td class="hide show_ad">{{$lead->ad}}</td>
                    <td class="hide show_keyword">{{$lead->keyword}}</td>
                  </tr>
                 @endforeach
                 
                </tbody>
              </table>
            </div>
        </div>

            <!-- /.box-body -->
       </div> <!--filter_table Ends--> 
       </div>  
       @endsection
   <!--adding_filter Ends-->
@section("filter")
    <div class="filters floatR">
      <div class="filter_button">
        <i class="fa fa-filter filter_icon"></i>
      </div>
      <div class="filter_overlay"></div>
      <div class="filter_content clearfix">
        <div class="dropdown filterby floatL">

          <select name="filter" id="filter" class="form-control filter-select">
          <option value="filterby" selected>Filter By</option>
            <option value="city">City</option>
            <option value="source">Source</option>
            <option value="campaign">Campaign</option>
            <option value="adgroup">Ad Group</option>
            <option value="keyword">Keyword</option>
            <option value="ad">Ad</option>
         </select> 
         <div class="error_msg">This field is required</div>
        </div>

        <div class="dropdown condition floatL">          
          <select name="condition" id="condition" class="form-control filter-condition">
            <option value="condition" selected>Condition</option>
            <option value="contain">Contain</option>
            <option value="doesnotcontain">Does not contain</option>
            <option value="is">Is</option>
            <option value="startwith">Start With</option>
            <option value="endwith">End With</option>          
          </select> 
          <div class="error_msg">This field is required</div>
        </div>
      
        <div class="having floatL">
          <div class="filter_having form-group form-group-sm label-floating">
           <label for="having" class="control-label">Having</label>
              <input type="text" class="form-control" name="having" id="having" required >  
              <div class="error_msg">This field is required</div>
          </div>
          <input type="hidden" name="daterangepicker_start2" id="daterangepicker_start2" value=""></input>
       <input type="hidden" name="daterangepicker_end2" id="daterangepicker_end2" value=""></input>
       </div>
      
       <div class="filter_applynow floatL">
        
         <button type="submit" class="btn btn-primary" id="apply">Apply</button> 
              </div>
      </div>      
    </div><!--filters Ends-->
  
 @endsection 
@section("footer")

    <script>
      $(function () {
       
        $('#table_data').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": false,
          "scrollX":true

        });
      });
    </script>
 <script type="text/javascript">
 
     $(function() {

         
           var ctx = document.getElementById("linechart").getContext("2d");
               
                //BAR CHART
                var data = {
                    type: 'line',
                    labels: [@foreach($leads_per_day_count as $l) "{{$l->creation}}",@endforeach],
                    datasets: [
                       
                        {
                            label: "Line Graph",
                            lineTension: 0.1,
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            borderJoinStyle: 'miter',
                            borderDash: [5, 5],
                             pointBorderWidth: 1,
                              pointHoverRadius: 5,
                              pointHoverBackgroundColor: "rgba(75,192,192,1)",
                              pointHoverBorderColor: "rgba(220,220,220,1)",
                              pointHoverBorderWidth: 2,
                              pointRadius: 1,
                              pointHitRadius: 10,
                              xPadding:6,
                              cornerRadius:6,
                              footerSpacing:1,
                              bodyFontFamily:"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                            data: [@foreach($leads_per_day_count as $l){{$l->total_leads}}, @endforeach]
                        }
                    ]
                };

          var MyNewChart = new Chart(ctx).Line(data,
            {
                  responsive : true,
                  maintainAspectRatio: false,

              });
          });

            // Chart.defaults.global.responsive = true;
            // 
                $(window).scroll(function(){
              var sticky = $('.main_header'),
                  scroll = $(window).scrollTop();

              if (scroll >= 70){
                  sticky.addClass('header_fixed');
                } 
              else {
                sticky.removeClass('header_fixed');
              }
            });

                function cb(start, end) {
                  $('#config-calendar span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
              }
              cb(moment().subtract(29, 'days'), moment());

              $('#config-calendar').daterangepicker({
                  ranges: {
                     'Today': [moment(), moment()],                    
                     'Week': [moment().subtract(6, 'days'), moment()],
                     'Month': [moment().subtract(29, 'days'), moment()],
                    
                  }
              }, cb);
  </script>

<script type="text/javascript">
    $(document).ready(function(){
      $('#config-calendar').click(function(){
      if($(".ranges >ul>li").hasClass("active")){
      $(".ranges >ul > li:nth-child(1)").attr("id","daily");
       $(".ranges >ul > li:nth-child(2)").attr("id","weekly");
        $(".ranges >ul > li:nth-child(3)").attr("id","monthly");
    }
    });
      });
    </script>
<script type="text/javascript">

  $(document).ready(function(){
  $(".applyBtn").on("click",function(){
    $("#dvloader").show();

  $(".esa").show();
  $(".esa1").hide();
  $(".weeklyreport").hide();
$(".monthlyreport").hide();
$(".dailyreport").hide();
$(".filterreports").hide();

  var i = $('input[name="daterangepicker_start"]').val();
  var e = $('input[name="daterangepicker_end"]').val();
  console.log(i);
  
  console.log($("input[id=daterangepicker_start1]").val($('input[name="daterangepicker_start"]').val()));
    console.log($("input[id=daterangepicker_end1]").val($('input[name="daterangepicker_end"]').val()));
     console.log($("input[id=daterangepicker_start2]").val($('input[name="daterangepicker_start"]').val()));
    console.log($("input[id=daterangepicker_end2]").val($('input[name="daterangepicker_end"]').val()));
  var data = "daterangepicker_start="+i+"&daterangepicker_end="+e;
  $.ajax({
    type:"GET",
    data:data,
    url:'{{ URL::to("/date1") }}',
    success:function(data)
      {
    $("#ack").html(data);
    $("#dvloader").hide();
      }
  })

  $("#ajaxform").on("submit", function(){
    $("#dvloader").show();

  var i = $('input[name="daterangepicker_start"]').val();
  var e = $('input[name="daterangepicker_end"]').val();
  //console.log(i);
  //
  
  console.log($("input[id=daterangepicker_start1]").val($('input[name="daterangepicker_start"]').val()));
    console.log($("input[id=daterangepicker_end1]").val($('input[name="daterangepicker_end"]').val()));

  var data = "daterangepicker_start1="+i+"&daterangepicker_end1="+e;
  $.ajax({
    type:"GET",
    data:data.serialize(),
    url:'{{ URL::to("/export/datewise") }}',
    success:function(data)
      {
    window.location = "/export/datewise";
    $("#dvloader").hide();
      }
  })
})

    $("#ajaxformcsv").on("submit", function(){
    $("#dvloader").show();

  var i = $('input[name="daterangepicker_start"]').val();
  var e = $('input[name="daterangepicker_end"]').val();
 
  
  console.log($("input[id=daterangepicker_start1]").val($('input[name="daterangepicker_start"]').val()));
    console.log($("input[id=daterangepicker_end1]").val($('input[name="daterangepicker_end"]').val()));

  var data = "daterangepicker_start1="+i+"&daterangepicker_end1="+e;
  $.ajax({
    type:"GET",
    data:data.serialize(),
    url:'{{ URL::to("/export/csv/datewise") }}',
    success:function(data)
      {
    window.location = "/export/csv/datewise";
    $("#dvloader").hide();
      }
  })
})

  });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $(".applyBtn").on("click",function(){

  $(".esa").show();
  $(".esa1").hide();
  $(".weeklyreport").hide();
$(".monthlyreport").hide();
$(".dailyreport").hide();
$(".filterreports").hide();
  $i = $('input[name="daterangepicker_start"]').val();
  $e = $('input[name="daterangepicker_end"]').val();
  var data = "daterangepicker_start="+$i+"&daterangepicker_end="+$e;
  $.ajax({
    type:"GET",
    data:data,
    url:'{{ URL::to("/date2") }}',
    success:function(data)
      {
    $("#total_leads").html(data);
      }
  })
  });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#config-calendar").on("click", function(){
    $("#daily").unbind("click").bind("click",function(){
      $("#dvloader").show();
    $.ajax({
    type:"GET",
    url:'{{ URL::to("/daily") }}',
    success:function(data)
      {
    $("#ack").html(data);
    $("#dvloader").hide();
      }
  });
      $.ajax({
    type:"GET",
    url:'{{ URL::to("/daily1") }}',
    success:function(data)
      {
    $("#total_leads").html(data);
    
      }
  });
  $(".esa").hide();
  $(".esa1").hide();
  $(".weeklyreport").hide();
$(".monthlyreport").hide();
$(".dailyreport").show();
$(".filterreports").hide();
    $("#export_daily").on("click", function(){
    $.ajax({
    type:"GET",
    async: false,
    url:'{{ URL::to("/export/daily") }}',
    success:function(data)
      {
   document.location.href ="/export/daily";
    }
  });
    });
      $("#export_daily_csv").on("click", function(){
    $.ajax({
    type:"GET",
    async: false,
    url:'{{ URL::to("/export/csv/daily") }}',
    success:function(data)
      {
   document.location.href ="/export/csv/daily";
    }
  })
    })
      });

    })
  });

</script>





<script type="text/javascript">
  $(document).ready(function(){
    $("#config-calendar").on("click", function(){
    $("#monthly").unbind("click").bind("click",function(){
      $("#dvloader").show();

    $.ajax({
    type:"GET",
    url:'{{ URL::to("/monthly") }}',
    success:function(data)
      {
        console.log("inside");
    $("#ack").html(data);
     $("#dvloader").hide();
      }
  });

  $.ajax({
    type:"GET",
    url:'{{ URL::to("/monthly1") }}',
    success:function(data)
      {
    $("#total_leads").html(data);
    
      }
  });

  $(".esa").hide();
  $(".esa1").hide();
  $(".dailyreport").hide();
  $(".weeklyreport").hide();
$(".monthlyreport").show();
$(".filterreports").hide();
      $("#export_monthly_csv").on("click", function(){
    $.ajax({
    type:"GET",
    url:'{{ URL::to("/export/csv/monthly") }}',
    success:function(data)
      {
   document.location.href = "/export/csv/monthly";
    }
  })
    });

         $("#export_monthly").on("click", function(){
    $.ajax({
    type:"GET",
    url:'{{ URL::to("/export/monthly") }}',
    success:function(data)
      {
   document.location.href = "/export/monthly";
    }
  });
      
    });



    })
});
  });
</script>


<script type="text/javascript">
  $(document).ready(function(){
    $("#config-calendar").on("click", function(){
    $("#weekly").unbind("click").bind("click",function(){
   $("#dvloader").show();
    $.ajax({
    type:"GET",
    url:'{{ URL::to("/weekly") }}',
    success:function(data)
      {
    $("#ack").html(data);
     $("#dvloader").hide();
      }
  });
      $.ajax({
    type:"GET",
    url:'{{ URL::to("/weekly1") }}',
    success:function(data)
      {
    $("#total_leads").html(data);
    
      }
  });
        $(".esa").hide();
  $(".esa1").hide();
  $(".dailyreport").hide();
  $(".monthlyreport").hide();
$(".weeklyreport").show();
     $(".filterreports").hide();
      $("#export_weekly").on("click",function(){
    $.ajax({
    type:"GET",
    url:'{{ URL::to("/export/weekly") }}',
    success:function(data)
      {
   document.location.href ="/export/weekly";
    
      }
  })
    });

  $("#export_weekly_csv").on("click",function(){
    $.ajax({
    type:"GET",
    url:'{{ URL::to("/export/csv/weekly") }}',
    success:function(data)
      {
   document.location.href ="/export/csv/weekly";
    
      }
  })
    });

    })
  });
  });
</script>

<script type="text/javascript">
$(document).ready(function(){
  
     $("#export").on("click", function(){
  $(".esa1").show();
    $(".esa").hide();
  $(".weeklyreport").hide();
$(".monthlyreport").hide();
$(".dailyreport").hide();
$(".filterreports").hide();
    //console.log("export");
    $.ajax({
      type:"GET",
      url:"{{ URL::to('/export/all')}}",
     success:function(data){
      window.location = "/export/all";
     }
    });
  });

  });
 
</script>

<script type="text/javascript">
$(document).ready(function(){
 
    $("#export_csv").on("click", function(){
    $(".esa1").show();
    $(".esa").hide();
    $(".weeklyreport").hide();
    $(".monthlyreport").hide();
    $(".dailyreport").hide();
    $(".filterreports").hide();
      //console.log("export");
      $.ajax({
        type:"GET",
        url:"{{ URL::to('/export/csv/all')}}",
       success:function(data){
        window.location = "/export/csv/all";
       }
      });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
  $("#apply").on("click", function(){

      $filter = $("#filter").val();
      $condition = $("#condition").val();
      $having  = $("#having").val();
   if($filter == "filterby" || $condition =="condition" || $having =="having"){
    $(".error_msg").show();
    return false;
   }
else if($filter.length > 0 && $condition.length > 0 && $having.length > 0){
  $(".error_msg").hide();
  $("#dvloader").show();
      var daterangepicker_start2 = $("input[name=daterangepicker_start2]").val();
     var daterangepicker_end2 = $("input[name=daterangepicker_end2]").val();
     if(daterangepicker_start2 != "" && daterangepicker_end2 != ""){
  $(".esa").hide();
  $(".esa1").hide();
  $(".weeklyreport").hide();
$(".monthlyreport").hide();
$(".dailyreport").hide();
$(".filterreports").show();
      var data = "filter="+$filter+"&condition="+$condition+"&having="+$having+"&daterangepicker_start2="+daterangepicker_start2+"&daterangepicker_end2="+daterangepicker_end2;
    
      console.log(data);
      $.ajax({
        type:"GET",
        data:data,
        url:"{{URL::to('/filter/leads')}}",

        success:function(data){
          $("#ack").html(data);
           $("#dvloader").hide();
        }
      });

 var f = document.getElementById("filter_val").value = $("#filter").val();
 var c = document.getElementById("condition_val").value = $("#condition").val();
var h =  document.getElementById("having_val").value = $("#having").val();
var data = "filter="+f+"&condition="+c+"&having="+h;
    


     $("#ajaxform1").on("submit", function(){

      var data = "filter="+f+"&condition="+c+"&having="+h;
      $.ajax({
        type:"GET",
        data:data.serialize(),
        url:"{{URL::to('/export/filter/datewise')}}",
        success:function(data){
          window.location="/export/filter/datewise";
        }

      });
    });

var f = document.getElementById("filter_val_csv").value = $("#filter").val();
 var c = document.getElementById("condition_val_csv").value = $("#condition").val();
var h =  document.getElementById("having_val_csv").value = $("#having").val();
      var data = "filter="+f+"&condition="+c+"&having="+h;

       $("#ajaxform1csv").on("submit", function(){

      var data = "filter="+f+"&condition="+c+"&having="+h;
console.log(data);
      $.ajax({
        type:"GET",
        data:data.serialize(),
        url:"{{URL::to('/export/csv/filter/datewise')}}",
        success:function(data){
          window.location="/export/csv/filter/datewise";
        }

      });
    });


    }
    else if(daterangepicker_start2 === "" && daterangepicker_end2 ==="")
    {
  $(".esa").hide();
  $(".esa1").hide();
  $(".weeklyreport").hide();
$(".monthlyreport").hide();
$(".dailyreport").hide();
$(".filterreports").show();
document.getElementById("filter_val").value = $("#filter").val();
 document.getElementById("condition_val").value = $("#condition").val();
 document.getElementById("having_val").value = $("#having").val();
      var data = "filter="+$filter+"&condition="+$condition+"&having="+$having;

      $.ajax({
        type:"GET",
        data:data,
        url:"{{URL::to('/filter/leads')}}",

        success:function(data){
          $("#ack").html(data);
           $("#dvloader").hide();
        }
      });



     var f = document.getElementById("filter_val_csv").value = $("#filter").val();
 var c = document.getElementById("condition_val_csv").value = $("#condition").val();
var h =  document.getElementById("having_val_csv").value = $("#having").val();
      var data = "filter="+f+"&condition="+c+"&having="+h;

       $("#ajaxform1csv").on("submit", function(){

      var data = "filter="+f+"&condition="+c+"&having="+h;
      $.ajax({
        type:"GET",
        data:data.serialize(),
        url:"{{URL::to('/export/csv/filter/datewise')}}",
        success:function(data){
          window.location="/export/csv/filter/datewise";
        }

      });
    });

    }
  }
    })
 
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#apply").on("click",function(event){
     
        $filter = $("#filter").val();
      $condition = $("#condition").val();
      $having  = $("#having").val();
     if($filter == "filterby" || $condition =="condition" || $having =="having"){
    $(".error_msg").show();

     return false;
   }
else if($filter.length > 0 && $condition.length > 0 && $having.length > 0){
    $("#dvloader").show();
    $(".error_msg").hide();
    var daterangepicker_start2 = $("input[name=daterangepicker_start2]").val();
       
       var daterangepicker_end2 = $("input[name=daterangepicker_end2]").val();
 if(daterangepicker_start2 != "" && daterangepicker_end2 != ""){
      var data = "filter="+$filter+"&condition="+$condition+"&having="+$having+"&daterangepicker_start2="+daterangepicker_start2+"&daterangepicker_end2="+daterangepicker_end2;
    
      console.log(data);

      $.ajax({
        type:"GET",
        data:data,
        url:"{{URL::to('/filter/total')}}",
        success:function(data){
          $("#total_leads").html(data);
           $("#dvloader").hide();
        }
      });
}
else if(daterangepicker_start2 =="" && daterangepicker_end2 ==""){
 $(".esa").hide();
  $(".esa1").hide();
  $(".weeklyreport").hide();
$(".monthlyreport").hide();
$(".dailyreport").hide();
$(".filterreports").show();
  var data = "filter="+$filter+"&condition="+$condition+"&having="+$having;
    
      console.log(data);

      $.ajax({
        type:"GET",
        data:data,
        url:"{{URL::to('/filter/total')}}",
        success:function(data){
          $("#total_leads").html(data);
           $("#dvloader").hide();
        }
      });
}
}
    })
  });
</script>
<script type="text/javascript">
   //Downloadbtn_dropdown
 $(".download-button").dropdown({"optionClass": "withripple"});
</script>
@endsection
