  <!--Graph Element Starts-->
    <div class="lead_chart_wrapper">
      <div class="panel">
        <div class="panel-body">           
            <canvas id="barchart" width="100%" height="200"></canvas>
        </div>
      </div>
    </div><!--Graph Element Ends-->
     <div id="dvloader" style="    display: none;
    text-align: center;
    margin-top: 0px;
    background-color: #fff; "><img src="/images/status.gif" > </div>
   <!--adding_filter Starts-->
              <div class="adding_filter">
                 <div class="filter_tabs floatR">
                    <label for="filter_parameters" class="control-label">Select Other Parameters</label>             
                    <select id="filter_parameters" multiple="multiple" class="multiselect">          
                      <option value="1" class="campaign">Campaigns</option>
                      <option value="2" class="adgroup">Ad Groups</option>
                      <option value="3" class="ad">Ad</option>
                      <option value="4" class="keyword">Keywords</option>            
                    </select>
                 
                 </div> <!--filter_tabs Ends-->     

       <div class="filter_table">
         <!-- /.box-header -->
            <div class="box-body">
              <table id="table_data" class="table table-bordered table-hover table-striped">
                <thead class="table-inverse">
                  <tr>
                    <th>Leads</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Source</th>
                    <th>Status</th>
                    <th id="show_td_1" class="hide_value">Campaign</th>
                    <th id="show_td_2" class="hide_value">Ad Groups</th>
                    <th id="show_td_3" class="hide_value">Ad</th>
                    <th id="show_td_4" class="hide_value">Keywords</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($daily_name_table as $lead)
                  <tr>
                    <td>{{$lead->id}}</td>
                    <td>{{$lead->creation}}</td>
                    <td>{{ucfirst($lead->customer_name)}}</td>
                    <td> {{ucfirst(strtolower($lead->city))}}</td>
                    <td>{{$lead->email}}</td>
                    <td>{{$lead->contact}}</td>
                    <td>{{$lead->source}}</td>
                    <td>01</td>
                     <td class="hide show_campaign">{{$lead->campaign}}</td>
                    <td class="hide show_adgroup">{{$lead->ad_group}}</td>
                    <td class="hide show_ad">{{$lead->ad}}</td>
                    <td class="hide show_keyword">{{$lead->keyword}}</td>
                  </tr>
                 @endforeach
                 
                </tbody>
              </table>
            </div>
        
  
    

    </div><!--adding_filter Ends-->
    </div><!--adding_filter Ends-->

  <script type="text/javascript">
      $.material.init();  
        $(".filter-select, .filter-condition").dropdown({"optionClass": "withripple"}); 
    </script>
     <script>
      $(function () { 
        $('#table_data').DataTable({
          "paging": true,
          "lengthChange": false,
          "ordering": false,
          "info": true,
          "autoWidth": false,
          "scrollX":true

        });
      });
    </script>
    <script type="text/javascript">
 
     $(function() {
                "use strict";
                //BAR CHART
                var data = {
                    type: 'line',
                    labels: [@foreach($daily_leads_graph as $la) "{{$la->creation}}",@endforeach],
                    datasets: [
                       
                        {
                            label: "My Second dataset",

                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            xPadding:6,
                            yPadding:6,
                            data: [@foreach($daily_leads_graph as $la){{$la->total_leads}}, @endforeach]
                        }
                    ]
                };

              new Chart(document.getElementById("barchart").getContext("2d")).Bar(data,{
                  responsive : true,
                  maintainAspectRatio: false,

              });

            });
            // Chart.defaults.global.responsive = true;
             $(window).scroll(function(){
              var sticky = $('.main_header'),
                  scroll = $(window).scrollTop();

              if (scroll >= 70){
                  sticky.addClass('header_fixed');
                } 
              else {
                sticky.removeClass('header_fixed');
              }
            });
  </script>
  <script type="text/javascript">
    $('#filter_parameters').change(function() {
            //console.log($(this).val());
       $(".hide").hide();                              
        if( $(this).val()){
         console.log($(this).val());
            for(var i=0; i < $(this).val().length; i++){
                if($(this).val()[i] == "1"){             
                    $("#show_campaign").fadeIn("fast")['show'](); 
                                     
                }
                else if($(this).val()[i] == "2"){
                        $("#show_adgroup").fadeIn("fast")['show']();
                         
                }
                 else if($(this).val()[i] == "3"){
                        $("#show_ad").fadeIn("fast")['show']();
                        
                }
                 else if($(this).val()[i] == "4"){
                        $("#show_keyword").show("fast")['show']();
                       
                }
            }
         }           
      });

    $('#filter_parameters').change(function() {});

    $('#filter_parameters').change(function() {
            //console.log($(this).val());
        
        }).multipleSelect({
            width: '100%'


        });
  </script>
@include("dashboard.filteradd")