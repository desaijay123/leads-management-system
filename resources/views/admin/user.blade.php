@extends('layouts.usersettings')
@section("content")
 <div class="col-md-10 floatR body_content">
      <div class="adduser_wrapper">
        <div class="card">
           <div class="card-header clearfix">
            <div class="user_text floatL"> User</div>
            <div class="userBtn_wrap floatR" data-toggle="modal" data-target="#modal-adduser">
              <i class="fa fa-plus-circle floatL addicon"></i>
              <div class="add_btn floatL">Add User</div>
            </div>
             <div class="userBtn_wrap floatR" data-toggle="modal" data-target="#modal-addpermission">
                <i class="fa fa-plus-circle floatL addicon"></i> 
                <div class="add_btn floatL">Add Brand</div> 
              </div>
              <div class="userBtn_wrap floatR" data-toggle="modal" data-target="#modal-addrole">
                <i class="fa fa-plus-circle floatL addicon"></i>
                <div class="add_btn floatL">Add Campaigns To Brand</div>
              </div> 
              <div class="userBtn_wrap floatR" data-toggle="modal" data-target="#modal-addcampaign">
                <i class="fa fa-plus-circle floatL addicon"></i>
                <div class="add_btn floatL">Add Campaign</div>
              </div>   
           </div>          
        </div>
      </div><!--adduser_wrapper-->

   <div class="add_new">
        <!-- Modal Contact -->
            <div class="modal fade modal-ext" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <!--Content-->
                    <div class="modal-content">
                        <!--Header-->
                        <div class="modal-header">
                          <button type="button" class="close floatR" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title floatL" id="myModalLabel">Add Campaigns To Brand	</h4>
                        </div>
                        <!--Body-->
                        <div class="modal-body">
                        <form id="ajaxcampaign">
                         <input type="hidden" name ="token" id="token" value="{{ csrf_token() }}">
                             <div class="md-form brandmd">                             
                                <select class="brand-select" name="brand_id" id="user_brand"> 
                                    <option value="" disabled selected>Choose your option</option>
                                  @foreach($brand as $b)
                                    <option value="{{$b->id}}">{{ucfirst($b->label)}}</option>
                                    @endforeach
                                </select>
                                 <label for="user_brand" class="role">Brand</label>
                                 <div class="error_msg" style="display: none;">This field is required</div>
                            </div>   

                             <div class="product_wrapper">   
                                <label for="product" class="control-label">Product</label>
                                <select name="campaign_name[]" id="product" multiple="multiple" class="multiselect">
                                @foreach($campaign as $c)
                                  <option value="{{$c->campaign_name}}">{{$c->campaign_name}}</option>
                                  @endforeach
                                </select>
                              </div>       

                            <div class="text-xs-center">
                                <button class="btn btn-primary" type="submit" id="role_submit_btn">Submit</button>
                            </div>
                            </form>
                        </div>                       
                    </div>
                    <!--/.Content-->
                </div>
            </div>
      </div><!--add_new_role Ends-->
       <div class="add_new">
        <!-- Modal Contact -->
            <div class="modal fade modal-ext" id="modal-addcampaign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <!--Content-->
                    <div class="modal-content">
                        <!--Header-->
                        <div class="modal-header">
                          <button type="button" class="close floatR" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title floatL" id="myModalLabel">Add Campaign</h4>
                        </div>
                        <!--Body-->
                        <div class="modal-body">
                        <form id="ajaxaddcampaign">
                         <input type="hidden" name ="token" id="token" value="{{ csrf_token() }}">
                           <div class="md-form">                            
                                <input type="text" name="name" id="campaign_name" class="form-control">
                                <label for="new_permission">Name</label>
                               <div class="error_msg" style="display: none;">This field is required</div>
                            </div>

                            <div class="md-form">                               
                                <input type="text" name="label" id="campaign_label" class="form-control">
                                <label for="new_label_permission">Label</label>
                               	<div class="error_msg" style="display: none;">This field is required</div>
                            </div>       

                            <div class="text-xs-center">
                                <button class="btn btn-primary" type="submit" id="role_submit_btn">Submit</button>
                            </div>
                            </form>
                        </div>                       
                    </div>
                    <!--/.Content-->
                </div>
            </div>
      </div><!--add_new_role Ends-->
	<div class="add_new">
        <!-- Modal Contact -->
            <div class="modal fade modal-ext" id="modal-addpermission" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <!--Content-->
                    <div class="modal-content">
                        <!--Header-->
                        <div class="modal-header">                        
                          <button type="button" class="close floatR" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                           <h4 class="modal-title floatL" id="myModalLabel">Add Brand</h4>
                        </div>
                        <!--Body-->
                        <div class="modal-body"> 
                        <form id="ajaxbrand">
                        {!! csrf_field() !!}                           
                            <div class="md-form">                            
                                <input type="text" name="name" id="new_brand" class="form-control">
                                <label for="new_permission">Name</label>
                               <div class="error_msg" style="display: none;">This field is required</div>
                            </div>

                            <div class="md-form">                               
                                <input type="text" name="label" id="new_label" class="form-control">
                                <label for="new_label_permission">Label</label>
                               <div class="error_msg" style="display: none;">This field is required</div>
                            </div>         

                            <div class="text-xs-center">
                                <button class="btn btn-primary" id="permission_submit_btn">Submit</button>
                            </div>
                            </form>
                        </div>                       
                    </div>
                    <!--/.Content-->
                </div>
            </div>
      </div><!--add_new_permission Ends-->
      <div class="add_new_user">
        <!-- Modal Contact -->
            <div class="modal fade modal-ext" id="modal-adduser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <!--Content-->
                    <div class="modal-content">
                        <!--Header-->
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">Add User</h4>
                        </div>

                        <!--Body-->
                        <div class="modal-body"> 
                         <form id="ajaxregister">   
                        <input type="hidden" name ="_token" id="token" value="{{ csrf_token() }}" required="required">                              
                            <div class="md-form">                            
                               <input type="text" name="name" id="form22" class="form-control" required="required">
                                <label for="form42">Name</label>
                                <div class="error_msg" style="display: none;">This field is required</div>
                            </div>

                            <div class="md-form">                               
                                <input type="text" name="email" id="form32" class="form-control" required="required">
                                <label for="form34">Email Id</label>
                                <div class="error_msg" style="display: none;">This field is required</div>
                            </div>
                             <div class="md-form">                              
                               <select class="brand-select" name="role_id" id="user_role"> 
                                    <option value="" disabled selected>Choose your option</option>
                                   @foreach($role as $r)

                                    <option value="{{$r->id}}">{{ucfirst($r->label)}}</option>
								
                                    @endforeach

                                </select>
                              <label for="user_role" class="role">Role</label>
                              <div class="error_msg" style="display: none;">This field is required</div>
                            </div>
                            <div class="md-form brandmd">                             
                                <select class="brand-select" name="brand_id" id="user_brand1"> 
                                    <option value="" disabled selected>Choose your option</option>
                                  @foreach($brand as $b)
                                    <option value="{{$b->id}}">{{ucfirst($b->label)}}</option>
                                    @endforeach
                                </select>
                                 <label for="user_brand" class="role">Brand</label>
                                 <div class="error_msg" style="display: none;">This field is required</div>
                            </div> 
                            
							 <div class="product_wrapper s" >   
                                <label for="product" class="control-label">Product</label>
                                <select name="campaign_id[]" id="product2" multiple="multiple" class="multiselect">
                           
                                </select>
                              </div> 
                                  

                            <div class="text-xs-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        </form>                       
                    </div>
                    <!--/.Content-->
                </div>
            </div>

      </div><!--add_new_user Ends-->

      <div class="user_add">
           <div class="user_addlist">
            <div class="card">
              <div class="card-header clearfix">
                  <div class="left_role floatL">
                      <h3>Name</h3>
                  </div>
                   <div class="left_permission floatL">
                      <h3>Roles</h3>
                  </div>
              </div>

               <div class="card-block">
                @foreach($user as $u)
                <form method="POST" action="/remove/user/{{$u->id}}" novalidate>
               {!! csrf_field()!!}
                   <div class="line_border clearfix">
                    <div class="left_permission floatL">
                        <p>{{ucfirst($u->name)}}</p>
                    </div>
                    <div class="right_permission floatL">
                      <div class="right_check_permission clearfix">
                       @foreach($u->roles as $role)
                        <div class="check_block floatL">
                         <fieldset class="form-group">
                            <label for="checkbox1"><input type="checkbox" name ="userroles" id="checkbox1" value="{{$role->label}}" required="required">
                            {{ucfirst($role->label)}}</label>
                          </fieldset>
                          <input type="hidden" name="userroles" value="{{$role->id}}">
                        </div>
                        @endforeach
                      </div>
                    </div>
                      @can("can_manage_users")
                    <div class="role_permission_btn floatL">
                      <button type="submit" class="btn floatL user_permission_btn">Remove</button>
                    </div>
                    @endcan
                  </div>
                  </form>
                  @endforeach
                  </div>
               </div>
            </div>
          </div>
      </div>

@endsection

@section("footer")
<script type="text/javascript">
$(document).ready(function(){


	 $('#product').multipleSelect({
      width: '100%'
     });

$('#product2').multipleSelect({
      width: '100%'
     });



$("#user_brand1").on("change",function(e){
 // alert(this.value);
		$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
});
	e.preventDefault();
var brand_id = $("#user_brand1").val();
	$.ajax({
				type:"GET",
				url:"{{URL::to('/admin/brand')}}",
				data:"brand_id="+brand_id,
				success:function(data){


				//console.log(data);
				$("#product2").empty();
				$(".s").find("li").empty();
				$.each(data, function(index,productobj){
					$("#product2").append('<option value="'+productobj.id+'">'+productobj.campaign_name+'</option>');
					$(".s").find("li").append('<label class="selected"><input type="checkbox" data-name="selectItemcampaign_name[]" value="'+productobj.campaign_name+'"><span>'+productobj.campaign_name+'</span></label>');
				});
				$('#product2').multipleSelect({
      width: '100%'
     });
				},		

	});
});

	$("#ajaxregister").on("submit", function(e){
		$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
});
		e.preventDefault();
		var name = $("#form22").val();
		var email = $("#form32").val();
		var user_brand = $("#user_brand").val();
		var user_role = $("#user_role").val();
		var campaign = $("#campaign_id").val();
		console.log(name);
		console.log(email);
		console.log(user_brand);
		console.log(user_role);
		if(name =="" || email =="" || user_brand == "" || user_role == ""  || campaign == ""){
			$(".error_msg").show();
		} 
		else{

			$(".error_msg").hide();

			$.ajax({
				type:"POST",
				url:"{{URL::to('/admin/register')}}",
				data:$("#ajaxregister").serialize(),
				success:function(data){
				$("#modal-adduser, .modal-backdrop.in").hide();
				$('#ajaxregister')[0].reset();
				 window.location = "/admin/user/info";
				},
				error   : function ( jqXhr, json, errorThrown ) 
        {
            var errors = jqXhr.responseJSON;
            var errorsHtml= '';
            $.each( errors, function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>'; 
            });
            toastr.error( errorsHtml , "Error " + jqXhr.status +': '+ errorThrown);
        }
		
	});
		}
});
});
</script>

<script type="text/javascript">
$(document).ready(function(){

	$("#ajaxbrand").on("submit", function(e){
		$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
});
		e.preventDefault();
	
		var brand = $("#new_brand").val();
		var brand_label = $("#new_label").val();
		if(brand == "" && brand_label ==""){
			$(".error_msg").show();
		}
		else{
			$(".error_msg").hide();
			$.ajax({
				type:"POST",
				url:"{{URL::to('/storebrand')}}",
				data:$("#ajaxbrand").serialize(),
				success:function(data){
					$("#modal-addpermission, .modal-backdrop.in").hide();
					$("#ajaxbrand")[0].reset();
				window.location = "/admin/user/info";

				},
					error   : function ( jqXhr, json, errorThrown ) 
        {
            var errors = jqXhr.responseJSON;
            var errorsHtml= '';
            $.each( errors, function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>'; 
            });
            toastr.error( errorsHtml , "Error " + jqXhr.status +': '+ errorThrown);
        }
			})
		}

	});
		
	});
</script>

<script type="text/javascript">
$(document).ready(function(){

	$("#ajaxcampaign").on("submit", function(e){
		$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
});
		e.preventDefault();
		var brand_id = $("#brand_id").val();
		//console.log(role);
		var campaign_name = $("#campaign_name").val();
		// console.log(role_label);
		
		if(brand_id == "" && campaign_name ==""){
			$(".error_msg").show();
		}
		else{
			$(".error_msg").hide();
			$.ajax({
				type:"POST",
				url:"{{URL::to('/storebrandscampaigns')}}",
				data:$("#ajaxcampaign").serialize(),
				success:function(data){
				$("#modal-addrole, .modal-backdrop.in").hide();
				$("#ajaxcampaign")[0].reset();
				 window.location = "/admin/user/info";
				},
					error   : function ( jqXhr, json, errorThrown ) 
        {
            var errors = jqXhr.responseJSON;
            var errorsHtml= '';
            $.each( errors, function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>'; 
            });
            toastr.error( errorsHtml , "Error " + jqXhr.status +': '+ errorThrown);
        }
			})
		}

	});
		
	});
</script>

<script type="text/javascript">
$(document).ready(function(){

	$("#ajaxaddcampaign").on("submit", function(e){
		$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
});
		e.preventDefault();
		var campaign_name = $("#campaign_name").val();
		var campaign_label = $("#campaign_label").val();
		
		if(campaign_name == "" && campaign_label ==""){
			$(".error_msg").show();
		}
		else{
			$(".error_msg").hide();
			$.ajax({
				type:"POST",
				url:"{{URL::to('/storecampaigns')}}",
				data:$("#ajaxaddcampaign").serialize(),
				success:function(data){
				$("#modal-addrole, .modal-backdrop.in").hide();
				$("#ajaxaddcampaign")[0].reset();
				 window.location = "/admin/user/info";
				},
					error   : function ( jqXhr, json, errorThrown ) 
        {
            var errors = jqXhr.responseJSON;
            var errorsHtml= '';
            $.each( errors, function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>'; 
            });
            toastr.error( errorsHtml , "Error " + jqXhr.status +': '+ errorThrown);
        }
			})
		}

	});
		
	});
</script>


@endsection