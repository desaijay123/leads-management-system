   @extends('layouts.usersettings')
   @section("content")
 
      <!--Roles & Permission-->
  <div class="clearfix"></div>    
    <div class="col-md-10 floatR body_content">
    
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
          <div class="top_roles_wrapper">      
        <!--Panel-->
        <div class="card"> 
        <!--  <div class="card-header clearfix">
              <div class="card_headingText floatL"> Roles for Permissions</div>
              
               <div class="userBtn_wrap floatR" data-toggle="modal" data-target="#modal-addpermission">
                <i class="fa fa-plus-circle floatL addicon"></i>
                <div class="add_btn floatL">Add Permission</div>
              </div>
             
      <div class="userBtn_wrap floatR" data-toggle="modal" data-target="#modal-addrole">
                <i class="fa fa-plus-circle floatL addicon"></i>
                <!-- <div class="add_btn floatL">Add Role</div> -->
              <!-- </div>    -->
 
            <!-- </div> -->

            <div class="card-block">             
                  <div class="role_wrapper floatL"> 
                    <label for="user_role" class="control-label">Role</label>  
                    <form method="POST" action="/admin/assign/roles/permissions"> 
	                    {!! csrf_field() !!}
	                    <select name="role_id" id="user_role" class="form-control">
	                     @foreach($roles as $role)
	                      <option value="{{$role->id}}">{{ucfirst($role->label)}}</option>
	                      @endforeach             
	                    </select>
	                  </div>
	                  <div class="permission_wrapper floatL">   
	                    <label for="user_permission" class="control-label">Permission</label>
	                    <select id="ms" name="permission_id[]" multiple="multiple" class="multiselect">
							@foreach($perm as $p)
							<option value="{{$p->id}}">{{ucfirst($p->label)}}</option>
							@endforeach
						</select>
	                  </div>
	                  <button type="submit" class="btn btn-primary floatL user_role_btn">Submit</button>
                  </form>
		</div>
    <!--/.Panel-->
         </div>
        </div>    <!--Roles for Permission Ends-->
   <div class="add_new">
        <!-- Modal Contact -->
            <div class="modal fade modal-ext" id="modal-addpermission" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <!--Content-->
                    <div class="modal-content">
                        <!--Header-->
                        <div class="modal-header">                        
                          <button type="button" class="close floatR" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                           <h4 class="modal-title floatL" id="myModalLabel">Add Permission</h4>
                        </div>
                        <!--Body-->
                        <div class="modal-body"> 
                        <form id="ajaxperm">
                        {!! csrf_field() !!}                           
                            <div class="md-form">                            
                                <input type="text" name="name" id="new_permission" class="form-control">
                                <label for="new_permission">Name</label>
                               <div class="error_msg" style="display: none;">This field is required</div>
                            </div>

                            <div class="md-form">                               
                                <input type="text" name="label" id="new_label" class="form-control">
                                <label for="new_label_permission">Label</label>
                               <div class="error_msg" style="display: none;">This field is required</div>
                            </div>         

                            <div class="text-xs-center">
                                <button class="btn btn-primary" id="permission_submit_btn">Submit</button>
                            </div>
                            </form>
                        </div>                       
                    </div>
                    <!--/.Content-->
                </div>
            </div>
      </div><!--add_new_permission Ends-->
      
       <div class="add_new">
        <!-- Modal Contact -->
            <div class="modal fade modal-ext" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <!--Content-->
                    <div class="modal-content">
                        <!--Header-->
                        <div class="modal-header">
                          <button type="button" class="close floatR" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title floatL" id="myModalLabel">Add Role</h4>
                        </div>
                        <!--Body-->
                        <div class="modal-body">
                        <form id="ajaxrole">
                         <input type="hidden" name ="token" id="token" value="{{ csrf_token() }}">

                            <div class="md-form">                            
                                <input type="text" name="name" id="new_role" class="form-control">
                                <label for="new_role">Name</label>
                                <div class="error_msg" style="display: none;">This field is required</div>
                            </div>

                            <div class="md-form">                               
                                <input type="text" name="label" id="label_role" class="form-control">
                                <label for="new_label_role">Label</label>
                                <div class="error_msg" style="display: none;">This field is required</div>
                            </div>         

                            <div class="text-xs-center">
                                <button class="btn btn-primary" id="role_submit_btn">Submit</button>
                            </div>
                            </form>
                        </div>                       
                    </div>
                    <!--/.Content-->
                </div>
            </div>
      </div><!--add_new_role Ends-->

        <div class="roles_permission">
            <div class="card">
              <div class="card-header clearfix">
                  <div class="left_role floatL">
                      <h3>Role</h3>
                  </div>
                   <div class="left_permission floatL">
                      <h3>Permission</h3>
                  </div>
              </div>
                 <div class="card-block clearfix">   
                 @foreach($roles as $r)
                 <form method="POST" action="/admin/revoke/permission/{{$r->id}}">
                {!! csrf_field() !!}                
                    <div class="left_permission floatL">
                        <p>{{ucfirst($r->label)}}</p>
                    </div>
                    <div class="right_permission floatL">
                      <div class="right_check_permission clearfix">
                      <input type="hidden" value="{{$i = 0}}"></input>
						      @foreach($r->permissions as $perms)
						     @if($r->name == "super_admin")
						     @if($i == 0)
						 		<input type="hidden" value=" {{$checkbox = "checkbox1"}}"></input>
						     @endif
						     @endif
						  @if($r->name == "brand_manager")
						  @if($i ==0)
						        <input type="hidden" value=" {{$checkbox = "checkbox2"}}"></input>
						   	@elseif($i == 1)
						   	<input type="hidden" value=" {{$checkbox = "checkbox3"}}"></input>
						     @endif
						     @endif
						     @if($r->name == "product_manager")
						     @if($i == 0)
						     <input type="hidden" value=" {{$checkbox = "checkbox5"}}"></input>
						     @endif
						     @endif
						     <input type="hidden" value="{{$i++}}">
                        <div class="check_block floatL">
                         <fieldset class="form-group">
                              <input name="perms[]" type="checkbox" id="{{$checkbox}}" value="{{$perms->id}}">
	                     <label for="{{$checkbox}}"> {{ucfirst($perms->label)}} </label>
                          </fieldset>
                        </div>
                        @endforeach
                      </div>
                    </div>
                   <!--  <div class="role_permission_btn floatL">
                      <button type="submit" class="btn floatL user_permission_btn">Remove</button>  
                    </div> -->
                    <div class="clearfix"></div>
					<div class="line_border clearfix"></div>
					</form>	
        			@endforeach		
               </div>

        </div>     
        </div>
        </div>  

@endsection

@section("footer")

<script type="text/javascript">
$(document).ready(function(){

	$("#ajaxrole").on("submit", function(e){
		$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
});
		e.preventDefault();
		var role = $("#new_role").val();
		//console.log(role);
		var role_label = $("#label_role").val();
		//console.log(role_label);
		
		if(role == "" && role_label ==""){
			$(".error_msg").show();
		}
		else{
			$(".error_msg").hide();
			$.ajax({
				type:"POST",
				url:"{{URL::to('/admin/roles/storerole')}}",
				data:$("#ajaxrole").serialize(),
				success:function(data){
				$("#modal-addrole, .modal-backdrop.in").hide();
				$("#ajaxrole")[0].reset();
				window.location = "/admin/roles/permissions";
				},
					error   : function ( jqXhr, json, errorThrown ) 
        {
            var errors = jqXhr.responseJSON;
            var errorsHtml= '';
            $.each( errors, function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>'; 
            });
            toastr.error( errorsHtml , "Error " + jqXhr.status +': '+ errorThrown);
        }
			})
		}

	});
		
	});
</script>

<script type="text/javascript">
$(document).ready(function(){

	$("#ajaxperm").on("submit", function(e){
		$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
});
		e.preventDefault();
	
		var permission = $("#new_permission").val();
		var permission_label = $("#new_label").val();
		if(permission == "" && permission_label ==""){
			$(".error_msg").show();
		}
		else{
			$(".error_msg").hide();
			$.ajax({
				type:"POST",
				url:"{{URL::to('/admin/permission/storeperm')}}",
				data:$("#ajaxperm").serialize(),
				success:function(data){
					$("#modal-addpermission, .modal-backdrop.in").hide();

					$("#ajaxperm")[0].reset();
				window.location = "/admin/roles/permissions";


				},
					error   : function ( jqXhr, json, errorThrown ) 
        {
            var errors = jqXhr.responseJSON;
            var errorsHtml= '';
            $.each( errors, function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>'; 
            });
            toastr.error( errorsHtml , "Error " + jqXhr.status +': '+ errorThrown);
        }
			})
		}

	});
		
	});
</script>
@endsection