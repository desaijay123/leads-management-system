   @extends('layouts.usersettings')
   @section("content")
  <div class="col-md-10 floatR body_content">

      <div class="profile_wrapper">
        <div class="card">
           <div class="card-header clearfix">
               <div class="profile_text floatL">{{ucfirst($name)}}</div>
            </div>
            <div class="card-block">
              <div class="clearfix"></div>
              <div class="profile_firstblock">
                <h3>Role</h3>
                <div class="profile_sub">@foreach($user->roles as $role)
				{{ucfirst($role->label)}}</div>
                <div class="sub_divider"></div>
              </div>
              <div class="profile_firstblock">
                <h3>Permission</h3>
                @foreach($role->permissions as $p)
                <div class="profile_sub">{{ucfirst($p->label)}}</div>
              @endforeach
                <div class="sub_divider"></div>
              </div>
                @endforeach
              <div class="profile_firstblock">
                <h3>Brand</h3>
                @if(Auth::user()->hasrole("super_admin"))
                 @foreach($brands as $brand)
                <div class="profile_sub">{{ucfirst($brand->name)}}</div>
                @endforeach
                @else
                 @foreach($user->brands as $brand)
                <div class="profile_sub">{{ucfirst($brand->name)}}</div>
                @endforeach
                @endif
              </div>
             <div class="sub_divider"></div>
              <div class="profile_firstblock">
                <h3>Brand</h3>
                @if(Auth::user()->hasrole("super_admin"))
                 @foreach($brands as $brand)
                <div class="profile_sub">{{ucfirst($brand->name)}}</div>
                @endforeach
                @else
                 @foreach($user->brands as $brand)
                <div class="profile_sub">{{ucfirst($brand->name)}}</div>
                @endforeach
                @endif
              </div>
            </div> 

        </div>
      </div><!--profile_wrapper-->
    </div>
    
   @endsection
