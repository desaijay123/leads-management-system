<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>LMS</title>
	<link rel="stylesheet" href="/css/reset.css">
	<link rel="stylesheet" href="/css/font-awesome.min.css">	
	<link rel="stylesheet" href="/css/fonts.css">
	<link rel="stylesheet" href="/css/bootstrap.css">
	<link href="/css/bootstrap-material-design.css" rel="stylesheet">
	<link href="/css/ripples.css" rel="stylesheet">
	<link href="/css/jquery.dropdown.css" rel="stylesheet">
	<link rel="stylesheet" href="/css/all.css">
	<script src="/js/modernizr.custom.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/responsive.css">

</head>
<body>
	
	<div class="container">
			<div id="selectbrand_page" class="selectbrand_page">
				<div class="login_logo">
					<img src="/images/pivotroots_logo.png" alt="pivotroots logo">
				</div>
				@if(Auth::user()->hasRole("super_admin"))
				<form action="/setclientlogin/" method="GET">
				 {!! csrf_field() !!}
					<div class="select_brand">
						<div class="form-group">
						    <!--label for="brand">Select Brand</label-->
						    <select name ="brand" id="brand" class="form-control selectbrand">
						      <option value="" disabled selected>Select Client</option>
						      @foreach($brands as $b)
						      <option value="{{$b->id}}">{{ucfirst($b->name)}}</option>			 
						      @endforeach
						    </select>
						</div>						
					 </div>
					 <button class="select_brand_submit" type="submit">Submit</button>
				</form>
				@endif
			</div>
	</div>


	
	<script src="/js/jquery-2.1.3.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/material.js"></script>	
	<script src="/js/ripples.js"></script>	
	<script src="/js/jquery.dropdown.js"></script>
	<script src="/js/custom.js"></script>
	<script type="text/javascript">
	  $.material.init();

	  //Select page
	
	 $("#brand").dropdown({"optionClass": "withripple"});  
	  
	</script>

</body>
</html>