@extends("layouts.app")

@section("content")

<div class="col-md-5" id="reset_page">
                <div class="login_logo">
                    <img src="/images/pivotroots_logo.png" alt="pivotroots logo">
                    <!--div class="powered_logo">
                        <span>Powered by</span> 
                        <img src="images/powered_logo.png" alt="pivotroots">
                    </div-->
                </div>

                <form class="form_login" method="POST" action="/password/reset">   
                  {!! csrf_field() !!}   
                   <input type="hidden" name="token" value="{{ $token }}"> 
                    <h1>Reset Password</h1>
                     <div class="form-group form-group-sm label-floating"> 
                     <i class="fa icon-user icon prefix"></i>                  
                        <label for="icon_email" class="control-label">Email Id</label>
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="icon_email" required>          
                     </div>           
                    <div class="form-group form-group-sm label-floating">
                        <i class="fa icon-lock icon prefix"></i>
                        <label for="icon_password" class="control-label">New Password</label>
                        <input type="password" name="password" class="form-control" id="icon_password" required>
                     </div>     

                     <div class="form-group form-group-sm label-floating">
                        <i class="fa icon-lock icon prefix"></i>
                        <label for="confirm_password" class="control-label">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="confirm_password" required>
                     </div>         
                     
                     <button class="login_btn" type="submit">Reset</button>
                     @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

                </form>
            </div>
@endsection