@extends("layouts.app")
@section("content")
          <div class="login_page" id="login_page">
                    <div class="login_logo">
                      <img src="images/pivotroots_logo.png" alt="pivotroots logo" class="img-responsive">
                    </div>
                <form class="form_login" method="POST" action="/auth/login">
                    {!! csrf_field() !!}
                    <div class="form-group form-group-sm label-floating">
          <i class="fa fa-user icon prefix"></i>
                        <label for="icon_email" class="control-label">Email Id</label>
                        <input type="email" name="email" class="form-control" id="icon_email" value="{{ old('email') }}" required>            
                     </div>
                     <div class="form-group form-group-sm label-floating">
                        <i class="fa fa-lock icon prefix"></i>
                        <label for="icon_password" class="control-label">Password</label>
                        <input type="password" name="password" class="form-control" id="icon_password" required>
                     </div>
                     <div class="bottom_line_login clearfix">
                        <div class="checkbox floatL">
                          <label for="remember-me">
                            <input type="checkbox" id="remember-me" name="remember"> 
                            <span class="remember_text">Remember me</span>
                          </label>                   
                        </div>
                        <div class="forget_password floatR">
                             <a href="/password/email">Forgot password?</a>
                        </div>
                     </div>
                        <button class="login_btn" type="submit">Login</button>
                       @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li class="alert alert-danger">{{ $error }}</li>
            @endforeach
        </ul>
    @endif
            </form>
            </div>
@endsection

