@extends("layouts.app")

@section("content")
<div class="forgot_page" id="forgot_page">
               <div class="login_logo">
                <img src="/images/pivotroots_logo.png" alt="pivotroots logo" class="img-responsive">
            </div>

                  <form class="form_login" method="POST" action="/password/email">  
                                 {!! csrf_field() !!}     
                                    <h1>Forgot Password</h1>            
                                    <div class="form-group form-group-sm label-floating">                   
                                        <label for="icon_email" class="control-label">Email Id</label>
                                        <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="icon_email" required>          
                                     </div>                 
                                     
                                     <button class="login_btn" type="submit">Submit</button>
                                     @if (count($errors) > 0)
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="alert alert-danger">{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
            
                        </form>
            </div>

@endsection