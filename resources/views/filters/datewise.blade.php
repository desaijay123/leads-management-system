
  <!--Graph Element Starts-->
    <div class="lead_chart_wrapper">
      <div class="panel">
        <div class="panel-body">           
            <canvas id="barchart" width="100%" height="200"></canvas>
        </div>
      </div>
    </div><!--Graph Element Ends-->

  <!--adding_filter Starts-->
    <div class="adding_filter">
       <div class="filter_tabs">
          <ul class="tab_filter">
           <li class="campaign">
             <a href="javascript:void(0)">Campaigns</a>
           </li>
           <li class="adgroup">
             <a href="javascript:void(0)">Ad Groups</a>
           </li>
           <li class="ad">
             <a href="javascript:void(0)">Ad</a>
           </li>
           <li class="keyword">
             <a href="javascript:void(0)">Keywords</a>
           </li>
         </ul>
         <div class="clearfix"></div>
       </div> <!--filter_tabs Ends-->      

       <div class="filter_table">
         <!-- /.box-header -->
            <div class="box-body">
              <table id="table_data" class="table table-bordered table-hover table-striped">
                <thead class="table-inverse">
                  <tr>
                    <th>Leads</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Source</th>
                    <th>Status</th>
                    <th class="show_campaign">Campaign</th>
                    <th class="show_adgroup">Ad Groups</th>
                    <th class="show_ad">Ad</th>
                    <th class="show_keyword">Keywords</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($filter_leads as $lead)
                  <tr>
                    <td>{{$lead->id}}</td>
                    <td>{{$lead->creation}}</td>
                    <td>{{$lead->customer_name}}</td>
                    <td> {{$lead->city}}</td>
                    <td>{{$lead->email}}</td>
                    <td>{{$lead->contact}}</td>
                    <td>{{$lead->source}}</td>
                    <td>01</td>
                    <td class="show_campaign">{{$lead->campaign}}</td>
                    <td class="show_adgroup">{{$lead->ad_group}}</td>
                    <td class="show_ad">{{$lead->ad}}</td>
                    <td class="show_keyword">{{$lead->keyword}}</td>
                  </tr>
                 @endforeach
                 
                </tbody>
              </table>
            </div>
        
            <!-- /.box-body -->
       </div> <!--filter_table Ends-->     
          

    </div><!--adding_filter Ends-->
     @yield("filter")
      <script>
      $(function () {
       
        $('#table_data').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": false,

        });
      });
    </script>
    
  <script type="text/javascript">
 
     $(function() {
                "use strict";
                //BAR CHART
                var data = {
                    type: 'line',
                    labels: [@foreach($filter_graph as $la) "{{$la->creation}}",@endforeach],
                    datasets: [
                       
                        {
                            label: "My Second dataset",

                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            data: [@foreach($filter_graph as $la){{$la->total_leads}}, @endforeach]
                        }
                    ]
                };

              new Chart(document.getElementById("barchart").getContext("2d")).Line(data,{
                  responsive : true,
                  maintainAspectRatio: false,

              });

            });
            // Chart.defaults.global.responsive = true;
                    $(window).scroll(function(){
              var sticky = $('.main_header'),
                  scroll = $(window).scrollTop();

              if (scroll >= 70){
                  sticky.addClass('header_fixed');
                } 
              else {
                sticky.removeClass('header_fixed');
              }
            });
  </script>
  <style type="text/css">
  .show_campaign,.show_adgroup,.show_ad,.show_keyword{display:none;}

  </style>
<script>
var flag = false;
$('.campaign').click(function(){  
    //$('.campaign,.adgroup,.ad,.keyword').removeClass('selected');
    if(flag == false){
      $('.show_campaign').show();   
      //$('.show_adgroup').hide();
      $(this).addClass('selected');
      flag = true;
    }else{
      $('.show_campaign').hide();
       
       $(this).removeClass('selected');
       flag = false;
    }
    
  });
  
  $('.adgroup').click(function(){ 
    
    //$('.campaign,.adgroup,.ad,.keyword').removeClass('selected');
    if(flag == false){
      $('.show_adgroup').show();
      $(this).addClass('selected');
      flag = true;
    }else{
      $('.show_adgroup').hide();
      
       $(this).removeClass('selected');
        flag = false;
    }
  });
  
  $('.ad').click(function(){    
    //$('.campaign,.adgroup,.ad,.keyword').removeClass('selected');
    if(flag == false){
      $('.show_ad').show();
      $(this).addClass('selected');
      //$('.show_campaign').hide();
      
      //$('.campaign').removeClass('selected');
      flag = true;
    }else{
      $('.show_ad').hide();
      $(this).removeClass('selected');
       flag = false;
    }
    
  });
  
  $('.keyword').click(function(){   
    //$('.campaign,.adgroup,.ad,.keyword').removeClass('selected');
    if(flag == false){
      $('.show_keyword').show();
      $(this).addClass('selected');
      //$('.show_campaign').hide();
      
      //$('.campaign').removeClass('selected');
      flag = true;
    }else{
      $('.show_keyword').hide();
      $(this).removeClass('selected');
       flag = false;
    }
    
  });
 </script>