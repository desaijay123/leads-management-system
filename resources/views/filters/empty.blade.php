
  <!--Graph Element Starts-->
   <!--  <div class="lead_chart_wrapper">
      <div class="panel">
        <div class="panel-body">           
            <canvas id="barchart" width="100%" height="200"></canvas>
        </div>
      </div>
    </div> --><!--Graph Element Ends-->

  <!--adding_filter Starts-->
    <div class="adding_filter">
        

       <div class="filter_table">
         <!-- /.box-header -->
            <div class="box-body">
              <table id="table_data" class="table table-bordered table-hover table-striped">
                <thead class="table-inverse">
                  <tr>
                    <th>Leads</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Source</th>
                    <th>Status</th>
                    <th class="show_campaign">Campaign</th>
                    <th class="show_adgroup">Ad Groups</th>
                    <th class="show_ad">Ad</th>
                    <th class="show_keyword">Keywords</th>
                  </tr>
                </thead>
                <tbody>
                
                  <tr>
                    <td>No data in the table</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="show_campaign"></td>
                    <td class="show_adgroup"></td>
                    <td class="show_ad"></td>
                    <td class="show_keyword"></td>
                  </tr>
               
                 
                </tbody>
              </table>
            </div>
        
            <!-- /.box-body -->
       </div> <!--filter_table Ends-->     
          

    </div><!--adding_filter Ends-->
     @yield("filter")
      <script>
      $(function () {
       
        $('#table_data').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": true,
          "autoWidth": false,

        });
      });
    </script>
   
  <style type="text/css">
  .show_campaign,.show_adgroup,.show_ad,.show_keyword{display:none;}

  </style>
<script>
var flag = false;
$('.campaign').click(function(){  
    //$('.campaign,.adgroup,.ad,.keyword').removeClass('selected');
    if(flag == false){
      $('.show_campaign').show();   
      //$('.show_adgroup').hide();
      $(this).addClass('selected');
      flag = true;
    }else{
      $('.show_campaign').hide();
       
       $(this).removeClass('selected');
       flag = false;
    }
    
  });
  
  $('.adgroup').click(function(){ 
    
    //$('.campaign,.adgroup,.ad,.keyword').removeClass('selected');
    if(flag == false){
      $('.show_adgroup').show();
      $(this).addClass('selected');
      flag = true;
    }else{
      $('.show_adgroup').hide();
      
       $(this).removeClass('selected');
        flag = false;
    }
  });
  
  $('.ad').click(function(){    
    //$('.campaign,.adgroup,.ad,.keyword').removeClass('selected');
    if(flag == false){
      $('.show_ad').show();
      $(this).addClass('selected');
      //$('.show_campaign').hide();
      
      //$('.campaign').removeClass('selected');
      flag = true;
    }else{
      $('.show_ad').hide();
      $(this).removeClass('selected');
       flag = false;
    }
    
  });
  
  $('.keyword').click(function(){   
    //$('.campaign,.adgroup,.ad,.keyword').removeClass('selected');
    if(flag == false){
      $('.show_keyword').show();
      $(this).addClass('selected');
      //$('.show_campaign').hide();
      
      //$('.campaign').removeClass('selected');
      flag = true;
    }else{
      $('.show_keyword').hide();
      $(this).removeClass('selected');
       flag = false;
    }
    
  });
 </script>