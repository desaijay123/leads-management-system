@extends("layouts.dashboard")
      @section("header")
      <!--middle_header Starts-->
        <div class="middle_header">
            <div class="middle_left floatL">
              <div class="totalimage_lead floatL"> 
                <img src="/images/totalleads.png" alt="totallead">
              </div>
              <div class="totallead_text floatL">
      <div id="total_leads"> <div class="totaltext">total leads - <span>{{ $leads_total }} </span></div></div>
              </div>
            </div><!--middle_left Ends-->

            <div class="middle_right floatR calendar_unit">
             <div id="config-calendar" class="pull-right" style="background: #fff; cursor: pointer; padding: 8px 0 8px 39px; border-bottom: 1px solid #ccc; width: 100%">
                <i class="glyphicon glyphicon-calendar fa fa-calendar" style="right: 318px;"></i>&nbsp;
                <span></span> <b class="caret"></b>
            </div>
            </div><!--middle_right Ends-->
  </div>
</header>
@endsection
  @section("content") 
  <div id="ack"> 

  <!--Graph Element Starts-->
    <div class="lead_chart_wrapper">
      <div class="panel">
        <div class="panel-body">           
            <canvas id="linechart" width="100%" height="200"></canvas>
        </div>
      </div>
    </div><!--Graph Element Ends-->
<div id="dvloader" style="    display: none;
    text-align: center;
    margin-top: 0px;
    background-color: #fff;"><img src="/images/status.gif" > </div>
    <!--Middle Content-->
    <div class="middle_content">
    
      <div class="middle_content_margin clearfix">
        <div class="middle_left_content floatL">
          <h3>Sources</h3>
          <div class="panel" id="scrollbar">
            <div class="panel-body"><ul>
              <input type="hidden" value="{{$j = 0}}"></input>
            @foreach($highest_source_leads as $total_leads)
           
      @if(strtolower($total_leads->source) === "search")
     <input type="hidden" value="{{$icon = "icon-Search-01"}}"></input>
     <input type="hidden" value="{{$class ="sources_search"}}"></input>
     @elseif(strtolower($total_leads->source) === "facebook")
     <input type="hidden" value="{{$icon = "fa fa-facebook"}}"></input>
     <input type="hidden" value="{{$class ="sources_fb"}}"></input>
    @elseif(strtolower($total_leads->source) === "google")
     <input type="hidden" value=" {{$icon = "fa fa-google"}}"></input>
     <input type="hidden" value="{{$class ="sources_gogle"}}"></input>
     @elseif(strtolower($total_leads->source) === "")
     <input type="hidden" value=" {{$icon = ""}}"></input>
     <input type="hidden" value="{{$class =""}}"></input>
    @endif
    <input type="hidden" value="{{$j++}}"> 
          <li>
            <div class="facebook_source clearfix">
            <div class="left_data floatL">
              <div class="leads_data">Leads 
              <br>
              <span>{{$total_leads->total_leads}}
             </span>
              </div>
            </div><!--left_data Ends-->
             <div class="right_data floatL">
              <div class="progress_data">
              <div class="sources_wrap clearfix">
                <div class="circle_icon floatL">
                <i class="{{$icon}} {{$class}} icon_text"></i>
                </div>
                <div class="sources_text floatL"> {{$total_leads->source}}
             </div>
              </div>
              <div class="progress">     
           <input type="hidden" value="{{$total_leads1 = $total_leads->total_leads}}"/>
          <input type="hidden"  value="{{ $total_leads1 / $leads_total}}"/>
        <input type="hidden" value="{{ number_format(($total_leads1/$leads_total) *100, 0)}}"/>
                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ (number_format(($total_leads1/$leads_total) *100, 1)-1)}}%">
                </div>
              </div>
              </div>                
            </div><!--left_data Ends-->
            </div><!--facebook_source Ends-->
          </li>
           @endforeach
        </ul>
            </div>            
          </div>
        </div><!--middle_left_content Ends-->

        <div class="middle_left_content floatL">
          <h3>Campaign</h3>
          <div class="panel" id="scrollbar1">
            <div class="panel-body"><ul>
          
            @foreach($highest_campaigns_leads as $campaigns)

        <li>
          <div class="campaign_source3 clearfix">
            <div class="left_data floatL">
              <div class="leads_data">Leads 
              <br>
              <span>{{$campaigns->total_campaign}}</span>
              </div>
            </div><!--left_data Ends-->
             <div class="right_data floatL">
              <div class="progress_data">
              <div class="sources_wrap">                      
                <div class="sources_text">{{$campaigns->campaign_name}}</div>
              </div>
               <div class="progress">
              <input type="hidden" value="{{$total_leads1 = $campaigns->total_campaign}}"/>
          <input type="hidden"  value="{{ $total_leads1 / $leads_total}}"/>
        <input type="hidden" value="{{ number_format(($total_leads1/$leads_total) *100, 0)}}"/>
                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{number_format(($total_leads1/$leads_total) *100, 0)}}%">
                </div>
              </div>
              </div>                
            </div><!--left_data Ends-->
           </div><!--mailer_source Ends-->
        </li>
        @endforeach
        </ul>
            </div>            
          </div>
        </div><!--middle_left_content Ends-->
            </div>            
          </div>
        
     

     
      <!--Top Lead Starts-->
      
      <div class="top_lead_container">
        <div class="topad_container">
          <h3>Top 5 Ad</h3>
          <div class="topad_panel">
            <div class="panel">
              <div class="panel-body">
                <div class="legend_topad clearfix">
               <input type="hidden" value="{{$i = 0}}"></input>
             @foreach($top5_ad as $top5)
                  @if($i == 0)
                  <input type="hidden" value=" {{$color = "#654312"}}"></input>
                   <input type="hidden" value=" {{$class = "veryhigh"}}"></input>
    @elseif($i == 1)
     <input type="hidden" value=" {{$color = "#8d6225"}}"></input>
      <input type="hidden" value=" {{$class = "high"}}"></input>
       
    @elseif($i == 2)
     <input type="hidden" value=" {{$color = "#b17b2f"}}"></input>
     <input type="hidden" value=" {{$class = "low"}}"></input>
     @elseif($i == 3)
      <input type="hidden" value=" {{$color = "#da983b"}}"></input>
      <input type="hidden" value=" {{$class = "normal"}}"></input>
     @elseif($i == 4)
      <input type="hidden" value=" {{$color = "#fac174"}}"></input>
       <input type="hidden" value=" {{$class = "verylow"}}"></input>
    @endif
    <input type="hidden" value="{{$i++}}">
                    <div class="legend floatL">
                      <div class="{{$class}} floatL legendcolor" style="background-color: {{$color}};     width: 12px;
    height: 12px;
    border-radius: 2px;
    margin: 10px;"></div>
                      <div class="legend_text floatL">{{$top5->ad}}</div>
                    </div>
                    @endforeach
                </div>
                <ul class="top_ad">
   <input type="hidden" value="{{$i = 0}}"></input>
                @foreach($top5_ad as $top5)
                  @if($i == 0)
                  <input type="hidden" value=" {{$color = "#654312"}}"></input>
       
    @elseif($i == 1)
     <input type="hidden" value=" {{$color = "#8d6225"}}"></input>
    @elseif($i == 2)
     <input type="hidden" value=" {{$color = "#b17b2f"}}"></input>
     @elseif($i == 3)
      <input type="hidden" value=" {{$color = "#da983b"}}"></input>
     @elseif($i == 4)
      <input type="hidden" value=" {{$color = "#fac174"}}"></input>
    @endif
    <input type="hidden" value="{{$i++}}">
      <input type="hidden" value="{{$total_leads1 = $top5->id}}"/>
          <input type="hidden"  value="{{ $total_leads1 / $leads_total}}"/>
        <input type="hidden" value="{{ number_format(($total_leads1/$leads_total) * 100, 0)}}"/>
                  <li style="width: {{ number_format(($total_leads1/$leads_total) *100, 0)}}%;">
                 
                    <div class="legend_container">
                      <div class="topad top_block1 "style=" background-color: {{$color}}">{{$top5->id}}</div>
                    </div>
                  </li>
                 @endforeach
                </ul>
              </div>
            </div>
          </div><!--top_panel Ends-->
        </div><!--topad_container Ends-->
        
        <div class="topadgroup_container">
          <h3>Top 5 Ad Group</h3>
          <div class="topadgroup_panel">
            <div class="panel">
              <div class="panel-body">
              <div class="legend_topad clearfix">
               <input type="hidden" value="{{$i = 0}}"></input>
               @foreach($top5_adgroup as $adgroup)
                  @if($i == 0)
                  <input type="hidden" value=" {{$color = "#5e6616"}}"></input>
                   <input type="hidden" value=" {{$class = "veryhigh"}}"></input>
    @elseif($i == 1)
     <input type="hidden" value=" {{$color = "#808b1e"}}"></input>
      <input type="hidden" value=" {{$class = "high"}}"></input>
       
    @elseif($i == 2)
     <input type="hidden" value=" {{$color = "#9fac26"}}"></input>
     <input type="hidden" value=" {{$class = "low"}}"></input>
     @elseif($i == 3)
      <input type="hidden" value=" {{$color = "#c1d030"}}"></input>
      <input type="hidden" value=" {{$class = "normal"}}"></input>
     @elseif($i == 4)
      <input type="hidden" value=" {{$color = "#e8fc24"}}"></input>
       <input type="hidden" value=" {{$class = "verylow"}}"></input>
    @endif
    <input type="hidden" value="{{$i++}}">
                    <div class="legend floatL">
                      <div class="{{$class}} floatL legendcolor" style="background-color: {{$color}};     width: 12px;
    height: 12px;
    border-radius: 2px;
    margin: 10px;"></div>
                      <div class="legend_text floatL">{{$adgroup->adgroup}}</div>
                    </div>
                    @endforeach
                </div>
                <ul class="top_ad_group">
                <input type="hidden" value="{{$i = 0}}"></input>
                @foreach($top5_adgroup as $adgroup)
                  @if($i == 0)
                  <input type="hidden" value=" {{$color = "#5e6616"}}"></input>
       
    @elseif($i == 1)
     <input type="hidden" value=" {{$color = "#808b1e"}}"></input>
    @elseif($i == 2)
     <input type="hidden" value=" {{$color = "#9fac26"}}"></input>
     @elseif($i == 3)
      <input type="hidden" value=" {{$color = "#c1d030"}}"></input>
     @elseif($i == 4)
      <input type="hidden" value=" {{$color = "#e8fc24"}}"></input>
    @endif
    <input type="hidden" value="{{$i++}}">
      <input type="hidden" value="{{$total_leads1 = $adgroup->id}}"/>
          <input type="hidden"  value="{{ $total_leads1 / $leads_total}}"/>
        <input type="hidden" value="{{ number_format(($total_leads1/$leads_total) *100, 0)}}"/>
                  <li style="width: {{ number_format(($total_leads1/$leads_total) *100, 0)}}%;">
                 
                    <div class="legend_container">
                
                      <div class="topad top_block1 "style=" background-color: {{$color}}">{{$adgroup->id}}</div>
                    </div>
                  </li>
                 @endforeach
                </ul>
              </div>
            </div>
          </div><!--top_panel Ends-->
        </div><!--topadgroup_container Ends-->

         <div class="topkeywords_container">
          <h3>Top 5 Keywords</h3>
          <div class="topkeywords_panel">
            <div class="panel">
              <div class="panel-body">
         <div class="legend_topad clearfix">
               <input type="hidden" value="{{$i = 0}}"></input>
             @foreach($top5_keyword as $keyword)
                  @if($i == 0)
                  <input type="hidden" value=" {{$color = "#11374e"}}"></input>
       
    @elseif($i == 1)
     <input type="hidden" value=" {{$color = "#184e6e"}}"></input>
       
    @elseif($i == 2)
     <input type="hidden" value=" {{$color = "#1f638c"}}"></input>
     @elseif($i == 3)
      <input type="hidden" value=" {{$color = "#287bae"}}"></input>
     @elseif($i == 4)
      <input type="hidden" value=" {{$color = "#3398d6"}}"></input>
    @endif
    <input type="hidden" value="{{$i++}}">
                    <div class="legend floatL">
                      <div class=" floatL legendcolor" style="background-color: {{$color}};     width: 12px;
    height: 12px;
    border-radius: 2px;
    margin: 10px;"></div>
                      <div class="legend_text floatL">{{$keyword->keyword}}</div>
                    </div>
                    @endforeach
                </div>
                <ul class="top_keywords">
                 <input type="hidden" value="{{$i = 0}}"></input>
                @foreach($top5_keyword as $keyword)
                  @if($i == 0)
                  <input type="hidden" value=" {{$color = "#11374e"}}"></input>
       
    @elseif($i == 1)
     <input type="hidden" value=" {{$color = "#184e6e"}}"></input>
       
    @elseif($i == 2)
     <input type="hidden" value=" {{$color = "#1f638c"}}"></input>
     @elseif($i == 3)
      <input type="hidden" value=" {{$color = "#287bae"}}"></input>
     @elseif($i == 4)
      <input type="hidden" value=" {{$color = "#3398d6"}}"></input>
    @endif
    <input type="hidden" value="{{$i++}}">
      <input type="hidden" value="{{$total_leads1 = $keyword->id}}"/>
          <input type="hidden"  value="{{ $total_leads1 / $leads_total}}"/>
        <input type="hidden" value="{{ number_format(($total_leads1/$leads_total) *100, 0)}}"/>
                  <li style="width:{{ (number_format(($total_leads1/$leads_total) *100,0 ))}}%;">
                  
                    <div class="legend_container">
                    
                      <div class="topad top_block1 "style="background-color: {{$color}} ">{{$keyword->id}}</div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div><!--top_panel Ends-->
        </div><!--topadgroup_container Ends-->

      </div><!--top_lead_container Ends-->
</div>


  @endsection

@section("footer")

     <script src="/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/js/overview.js"></script>
    <script type="text/javascript">
      $.material.init();  
      $(".filter-select, .filter-condition").dropdown({"optionClass": "withripple"}); 
        
    </script>
   
    <script type="text/javascript">
     $(function() {
                "use strict";
                //BAR CHART
                var data = {
                    type: 'line',
                    labels: [@foreach($month_graph as $month)"{{$month->month}}", @endforeach],
                    datasets: [
                      
                        {
                            label: "My Second dataset",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,1)",
                            pointColor: "rgba(151,187,205,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,1)",
                            xPadding:6,
                            yPadding:6,
                            data: [@foreach($month_graph as $month){{$month->id}},@endforeach]
                        }
                    ]
                };

              new Chart(document.getElementById("linechart").getContext("2d")).Line(data,{
                  responsive : true,
                  maintainAspectRatio: false,
                  scaleStartValue:0,


              });

              //Calendar Starts
            function cb(start, end) {
                  $('#config-calendar span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
              }
              cb(moment().subtract(29, 'days'), moment());

              $('#config-calendar').daterangepicker({
                  ranges: {
                     'Today': [moment(), moment()],                    
                     'Week': [moment().subtract(6, 'days'), moment()],
                     'Month': [moment().subtract(29, 'days'), moment()],
                    
                  }
              }, cb);

            });
            
             $(window).scroll(function(){
              var sticky = $('.main_header'),
                  scroll = $(window).scrollTop();

              if (scroll >= 70){
                  sticky.addClass('header_fixed');
                } 
              else {
                sticky.removeClass('header_fixed');
              }
            });

  </script>
 <script type="text/javascript">
    $(document).ready(function(){
      $('#config-calendar').click(function(){
      if($(".ranges >ul>li").hasClass("active")){
      $(".ranges >ul > li:nth-child(1)").attr("id","daily");
       $(".ranges >ul > li:nth-child(2)").attr("id","weekly");
        $(".ranges >ul > li:nth-child(3)").attr("id","monthly");
    }
    });
      });
    </script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#config-calendar").on("click",function(){
    $("#daily").unbind("click").bind("click",function(){
 $("#dvloader").show();
    $.ajax({
    type:"GET",
    url:'{{ URL::to("/overview/daily") }}',
    success:function(data)
      {
    $("#ack").html(data);
           
    $("#scrollbar,#scrollbar1").mCustomScrollbar({
      setHeight:260,
      theme:"minimal-dark"
    });
    $("#dvloader").hide();
      }
  });

    $.ajax({
    type:"GET",
    url:'{{ URL::to("/overview/total") }}',
    success:function(data)
      {
    $("#total_leads").html(data);
           
    $("#scrollbar,#scrollbar1").mCustomScrollbar({
      setHeight:260,
      theme:"minimal-dark"
    });
    $("#dvloader").hide();
      }
  });


    })
       
  });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#config-calendar").on("click",function(){
    $("#monthly").unbind("click").bind("click",function(){
             $("#dvloader").show();

    $.ajax({
    type:"GET",
    url:'{{ URL::to("/overview/monthly") }}',
    success:function(data)
      {
    $("#ack").html(data);
    $("#scrollbar,#scrollbar1").mCustomScrollbar({
      setHeight:260,
      theme:"minimal-dark"
    });
      $("#dvloader").hide();
      }
  });
     $.ajax({
    type:"GET",
    url:'{{ URL::to("/overview/monthlyleadstotal") }}',
    success:function(data)
      {
    $("#total_leads").html(data);
    $("#scrollbar,#scrollbar1").mCustomScrollbar({
      setHeight:260,
      theme:"minimal-dark"
    });
      $("#dvloader").hide();

  
      }
  });
    })
  });
  });
</script>


<script type="text/javascript">
  $(document).ready(function(){
    $("#config-calendar").on("click", function(){
    $("#weekly").unbind("click").bind("click",function(){
     $("#dvloader").show();

    $.ajax({
    type:"GET",
    url:'{{ URL::to("/overview/weekly") }}',
    success:function(data)
      {
    $("#ack").html(data);
    $("#scrollbar,#scrollbar1").mCustomScrollbar({
      setHeight:260,
      theme:"minimal-dark"
    });
      $("#dvloader").hide();

      }
  });

      $.ajax({
    type:"GET",
    url:'{{ URL::to("/overview/weeklytotalfilter") }}',
    success:function(data)
      {
    $("#total_leads").html(data);
   
   $("#dvloader").hide();
      }
  });
    })
  });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
  $(".applyBtn").on("click",function(){

  $i = $('input[name="daterangepicker_start"]').val();
  $e = $('input[name="daterangepicker_end"]').val();
  var data = "daterangepicker_start="+$i+"&daterangepicker_end="+$e;
  $.ajax({
    type:"GET",
    data:data,
    url:'{{ URL::to("/overview/datewisetotal") }}',
    success:function(data)
      {
    $("#total_leads").html(data);
      }

  })
  });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
  $(".applyBtn").on("click",function(){
          $("#dvloader").show();

  $i = $('input[name="daterangepicker_start"]').val();
  $e = $('input[name="daterangepicker_end"]').val();
  var data = "daterangepicker_start="+$i+"&daterangepicker_end="+$e;
  console.log(data);
  $.ajax({
    type:"GET",
    data:data,
    url:'{{ URL::to("/overview/datewise") }}',

    success:function(data)
      {

    $("#ack").html(data);
      $("#scrollbar,#scrollbar1").mCustomScrollbar({
      setHeight:260,
      theme:"minimal-dark"
    });
    $("#dvloader").hide();

      }

  })
  });
  });
</script>
  @endsection