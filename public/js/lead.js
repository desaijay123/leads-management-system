$(document).ready(function($) {
	//Menu Active Click

	$('.menu_left .nav li').click(function(){
    	$('.menu_left .nav li').removeClass("active");
    	$(this).addClass("active");
    });

	//Calendar Tab Active

	$('.fc-button-group button').click(function(){
    	$('.fc-button-group button').removeClass("fc_button_active");
    	$(this).addClass("fc_button_active");
    });

//Calendar Starts
  $('#config-calendar').daterangepicker({
    "startDate": "05/17/2016",
    "endDate": "05/23/2016"
	}, function(start, end, label) {
	  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
	});


  //On Scroll Header Fixed

	$(window).scroll(function(){
	  var sticky = $('.main_header'),
	      scroll = $(window).scrollTop();

	  if (scroll >= 70){
	  		sticky.addClass('header_fixed');
			} 
	  else {
	  	sticky.removeClass('header_fixed');
	  }
	});



//Filters Click

	var flag = false;
	$('.filter_button').click(function(){
	    if( flag == false){
	       $('.filter_overlay').show('200');
	       $('.filter_button').css('background-color','white')
	       $(this).find('i').toggleClass('fa-filter fa-close')
	        $('.filter_content').show().addClass('slideLeft');
	         // Add more code
	      	
			  
	       flag = true;
	    }
	    else{
	       $('.filter_overlay').hide('200');
	       $('.filter_content').fadeOut();
	        $(this).find('i').toggleClass('fa-close fa-filter')
	       $('.filter_button').css('background-color','#cada2b')
	       // Add more code

			  
	       flag = false;
	    }
	});


	//Filters Tabs Click

	$('.tab_filter li').click(function(){		
    	$('.tab_filter li').removeClass('selected');
		$(this).addClass('selected');
    });


});//document ready function Ends